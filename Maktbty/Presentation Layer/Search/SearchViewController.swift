//
//  SearchViewController.swift
//  Maktbty
//
//  Created by Omar Ibrahim on 8/28/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {
    
    var searchedBooks: [SearchedBook] = []
    var searchedAuthors: [Authors] = []
    var searchedType: String = ""

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "نتائج البحث"
        configureCollectionView()
        // Do any additional setup after loading the view.
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "GenericBooksScreensCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GenericBooksScreensCollectionViewCell")
        collectionView.register(UINib(nibName: "AuthorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AuthorCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchedType == "Home" {
            return searchedBooks.count > 0 ? searchedBooks.count : 0
        } else if searchedType == "Author" {
            return searchedAuthors.count > 0 ? searchedAuthors.count : 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if searchedType == "Home" {
            if let cell: GenericBooksScreensCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericBooksScreensCollectionViewCell", for: indexPath) as? GenericBooksScreensCollectionViewCell {
                
                cell.setData(text: searchedBooks[indexPath.row].title)
                cell.favouriteButton.setTitle(String(searchedBooks[indexPath.row].heartsCount), for: .normal)
                cell.commentsButton.setTitle(String(searchedBooks[indexPath.row].commentsCount), for: .normal)
                cell.bookImage.loadImageFromUrl(imageUrl: searchedBooks[indexPath.row].image)
                cell.ratingsCosmosView.text = String(searchedBooks[indexPath.row].averageRate)
                cell.ratingsCosmosView.rating = Double(searchedBooks[indexPath.row].averageRate)
                
                return cell
            }
        } else if searchedType == "Author" {
            if let cell: AuthorCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AuthorCollectionViewCell", for: indexPath) as? AuthorCollectionViewCell {
                
                cell.setData(text: searchedAuthors[indexPath.row].name)
                cell.authorImage.loadImageFromUrl(imageUrl: searchedAuthors[indexPath.row].image)
                cell.averageRateCosmosView.text = String(searchedAuthors[indexPath.row].averageRate)
                cell.averageRateCosmosView.rating = Double(searchedAuthors[indexPath.row].averageRate)
                
                return cell
            }
        } else if searchedType == "Favourite" {
            if let cell: GenericBooksScreensCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericBooksScreensCollectionViewCell", for: indexPath) as? GenericBooksScreensCollectionViewCell {
                
                cell.setData(text: searchedBooks[indexPath.row].title)
                cell.favouriteButton.setTitle(String(searchedBooks[indexPath.row].heartsCount), for: .normal)
                cell.commentsButton.setTitle(String(searchedBooks[indexPath.row].commentsCount), for: .normal)
                cell.bookImage.loadImageFromUrl(imageUrl: searchedBooks[indexPath.row].image)
                cell.ratingsCosmosView.text = String(searchedBooks[indexPath.row].averageRate)
                cell.ratingsCosmosView.rating = Double(searchedBooks[indexPath.row].averageRate)
                
                return cell
            }
        }
        
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 40 )/3.0
        return CGSize (width: width, height: width * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 10 , left: 0, bottom: 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.searchedType == "Home" {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let destinationVC = sb.instantiateViewController(
                withIdentifier: "AboutBookViewController") as?  AboutBookViewController
            guard let id = searchedBooks[indexPath.row].id else {  return }
            destinationVC!.bookId = id
            self.navigationController?.pushViewController(destinationVC!, animated: true)
            
        } else if self.searchedType == "Author" {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let destinationVC = sb.instantiateViewController(
                withIdentifier: "AboutAuthorViewController") as?  AboutAuthorViewController
            guard let id = searchedAuthors[indexPath.row].id else {  return }
            destinationVC!.authorId = id
            self.navigationController?.pushViewController(destinationVC!, animated: true)
        }
    }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
}
