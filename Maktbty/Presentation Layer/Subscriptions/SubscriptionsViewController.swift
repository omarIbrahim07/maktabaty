//
//  SubscriptionsViewController.swift
//  Maktbty
//
//  Created by Gado on 5/29/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class SubscriptionsViewController: BaseViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var monthlyBundle: UIButton!
    @IBOutlet weak var annualBundle: UIButton!
    @IBOutlet weak var policyButton: UIButton!
    @IBOutlet weak var subscribeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView() {
        textLabel.textAlignment = NSTextAlignment.justified
        
        monthlyBundle.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        annualBundle.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        policyButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        subscribeButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        monthlyBundle.reversesTitleShadowWhenHighlighted = true
        navigationItem.title = "الاشتراكات"
    }

    @IBAction func policyButtonIsPressed(_ sender: Any) {
        
        guard let url = URL(string: "http://demo.maktbti.com/privacy.php") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func monthlyBundleButton(_ sender: Any) {
        
    }
    
    @IBAction func annualBundleButton(_ sender: Any) {
        
    }
    

}
