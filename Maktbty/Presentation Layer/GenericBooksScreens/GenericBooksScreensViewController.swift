//
//  FreeBooksViewController.swift
//  Maktbty
//
//  Created by Gado on 6/16/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class GenericBooksScreensViewController: BaseViewController {

    @IBOutlet weak var booksSearchBar: UISearchBar!
    
    var myBooks: [BooksByCategory] = []
    var categoryId : Int = 0
    var categoryName : String = ""
    var flag : Int = 0

    @IBOutlet weak var viewContainingToolBar: UIView!
    @IBOutlet weak var genericBooksScreensCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureCollectionView()
        
        
        switch flag {
        case 1:
            getFreeSubscribtionSaleBooks()
            self.title = "كتب مجانية"
        case 2:
            getFreeSubscribtionSaleBooks()
            self.title = "كتب للبيع"
        case 3:
            getFreeSubscribtionSaleBooks()
            self.title = "كتب الاشتراك"
        case 4:
            self.title = "مكتبتي"
            getPaidBooks()
            // لسة فاضل حتة ال api اللي يتنده

        
        default:
            getBooks()
            self.title = categoryName


        }

        // Do any additional setup after loading the view.
    }
    
  
    
    
    func configureView() {
        //MARK: Reuse the view of the toolbar
        let reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
        reusableView.frame = self.viewContainingToolBar.bounds
        self.viewContainingToolBar.addSubview(reusableView)
    }
    
    func configureCollectionView() {
        self.genericBooksScreensCollectionView.delegate = self
        self.genericBooksScreensCollectionView.dataSource = self
        self.genericBooksScreensCollectionView.register(UINib(nibName: "GenericBooksScreensCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GenericBooksScreensCollectionViewCell")
    }
 
    
    
    
    func getBooks() {
        
        startLoading()
        
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else { return }
        
        CategoriesAPIManager().getBooks(categoryId: categoryId, userId: userID, basicDictionary: [:], onSuccess: { (books) in
            
            self.stopLoadingWithSuccess()

            self.myBooks = books
            print(self.myBooks)
            self.genericBooksScreensCollectionView.reloadData()
        }) { (error) in
            self.stopLoadingWithError(error: error)
            print(error)
        }
    }
    
    func getFreeSubscribtionSaleBooks() {
        
        startLoading()
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else { return }

        CategoriesAPIManager().getFreeSubscribtionSaleBooks(userId: userID, typeId: flag, basicDictionary: [:], onSuccess: { (books) in
            
            self.stopLoadingWithSuccess()
            self.myBooks = books
            print(self.myBooks)
            self.genericBooksScreensCollectionView.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func getPaidBooks() {
        
        startLoading()
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else { return }
        
        let params: [String : AnyObject] = [
            "user_id" : userID as AnyObject
        ]
        
        CategoriesAPIManager().getPaidBooks(basicDictionary: params, onSuccess: { (books) in
            
            self.stopLoadingWithSuccess()
            self.myBooks = books
            print(self.myBooks)
            self.genericBooksScreensCollectionView.reloadData()
        }) { (error) in
            print(error)
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GenericBooksScreensViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myBooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = genericBooksScreensCollectionView.dequeueReusableCell(withReuseIdentifier: "GenericBooksScreensCollectionViewCell", for: indexPath) as! GenericBooksScreensCollectionViewCell
        cell.setData(text: myBooks[indexPath.row].title)
        cell.favouriteButton.setTitle(String(myBooks[indexPath.row].heartsCount), for: .normal)
        cell.commentsButton.setTitle(String(myBooks[indexPath.row].commentsCount), for: .normal)
        cell.bookImage.loadImageFromUrl(imageUrl: myBooks[indexPath.row].image)
        cell.ratingsCosmosView.text = String(myBooks[indexPath.row].averageRate)
        cell.ratingsCosmosView.rating = Double(myBooks[indexPath.row].averageRate)

        return cell
    }
    
}

extension GenericBooksScreensViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 40 )/3.0
        return CGSize (width: width, height: width * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 10 , left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AboutBookViewController") as?  AboutBookViewController
        guard let id = myBooks[indexPath.row].id else {  return }
        destinationVC!.bookId = id
        self.navigationController?.pushViewController(destinationVC!, animated: true)
        
    }
}
