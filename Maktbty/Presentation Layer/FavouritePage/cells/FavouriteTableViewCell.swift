//
//  FavouriteTableViewCell.swift
//  Maktbty
//
//  Created by Gado on 6/2/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

protocol favouriteButtonTableViewCellDelegate {
    func didFavouriteButtonPressed(tag: Int)
}

class FavouriteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var favouriteItemImage: UIImageView!
    
    var favouriteButtonDelegate: favouriteButtonTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureView()
    }
    
    func configureView() {
//        starButton.isMultipleSelectionEnabled = true
        favouriteItemImage.layer.borderWidth = 1
        favouriteItemImage.layer.masksToBounds = false
        favouriteItemImage.layer.borderColor = UIColor.black.cgColor
        favouriteItemImage.layer.cornerRadius = favouriteItemImage.frame.height/2
        favouriteItemImage.clipsToBounds = true

    }
    
    //MARK:- Delegate Helpers
    func didFavouriteButtonPressed(tag: Int) {
        if let delegateValue = favouriteButtonDelegate {
            delegateValue.didFavouriteButtonPressed(tag: tag)
        }
    }
    
    @IBAction func starButtonIsPressed(_ sender: Any) {
        didFavouriteButtonPressed(tag: starButton.tag)
        print(tag)
        print("Show Button")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
