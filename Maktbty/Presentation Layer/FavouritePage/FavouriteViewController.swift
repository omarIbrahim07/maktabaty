//
//  FavouriteViewController.swift
//  Maktbty
//
//  Created by Gado on 6/2/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class FavouriteViewController: BaseViewController {
    
    @IBOutlet weak var favouriteTableView: UITableView!
    
    var favouriteBooks: [SearchedBook] = []
    
    var memberId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTableView()

        getFavouriteBooks()
    }
    
    func configureView() {
        self.title = "الكتب المفضلة"
    }
  
    func configureTableView() {
        favouriteTableView.delegate = self
        favouriteTableView.dataSource = self
        favouriteTableView.register(UINib(nibName: "FavouriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavouriteTableViewCell")
    }
    
    func getFavouriteBooks() {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        let parameters: [String : AnyObject] = [
            "mem_id" : self.memberId as AnyObject
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().getFavouriteBooks(basicDictionary: parameters , onSuccess: { (favouriteBooks) in
            
            self.stopLoadingWithSuccess()
            
            self.favouriteBooks = favouriteBooks
            self.favouriteTableView.reloadData()
            
        }) { (error) in
            print("error")
        }
    }
    
    func unfavouriteIsPressed(tag: Int) {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        guard let bookId = self.favouriteBooks[tag].id, bookId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الكتاب"
            showError(error: apiError)
            return
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : self.memberId as AnyObject,
            "like_id" : bookId as AnyObject,
            "type" : "unlike" as AnyObject
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().unfavouriteBook(basicDictionary: parameters , onSuccess: { (message) in
            
            self.stopLoadingWithSuccess()
            
            self.getFavouriteBooks()
            self.favouriteTableView.reloadData()
            
        }) { (error) in
            print("error")
        }
    }

}


extension FavouriteViewController :  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favouriteBooks.count > 0 ? self.favouriteBooks.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: FavouriteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FavouriteTableViewCell", for: indexPath) as? FavouriteTableViewCell {
            
            cell.bookTitleLabel.text = favouriteBooks[indexPath.row].title
            cell.favouriteItemImage.loadImageFromUrl(imageUrl: favouriteBooks[indexPath.row].image)
            
            cell.starButton.tag = indexPath.row
            cell.favouriteButtonDelegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension FavouriteViewController: favouriteButtonTableViewCellDelegate {
    
    func didFavouriteButtonPressed(tag: Int) {
        unfavouriteIsPressed(tag: tag)
    }
}
