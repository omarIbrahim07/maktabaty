//
//  AccountSettingsViewController.swift
//  Maktbty
//
//  Created by Gado on 5/28/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import DLRadioButton
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos


class AccountSettingsViewController: BaseViewController {
    
    var myImage: UIImage? = UIImage()
    var oldEmail = UserDefaultManager.shared.currentUser?.email

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reenterPasswordTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var showPassword: DLRadioButton!
    @IBOutlet var firstCheckBox: DLRadioButton!
    @IBOutlet var secondCheckBox: DLRadioButton!
    @IBOutlet var thirdCheckBox: DLRadioButton!
    @IBOutlet var fourthCheckBox: DLRadioButton!
    
    @IBOutlet var fifthCheckBox: DLRadioButton!
    
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        appendData()
        // Do any additional setup after loading the view.
    }
    
    
    func configureView() {
        emailTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        usernameTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        oldPasswordTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        newPasswordTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        reenterPasswordTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        showPassword.isMultipleSelectionEnabled = true
        
        firstCheckBox.isMultipleSelectionEnabled = true
        secondCheckBox.isMultipleSelectionEnabled = true
        
        thirdCheckBox.isMultipleSelectionEnabled = true
        fourthCheckBox.isMultipleSelectionEnabled = true
        fifthCheckBox.isMultipleSelectionEnabled = true
        
        saveButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
    }
    
    func appendData() {
        
        if let userName = UserDefaultManager.shared.currentUser?.name {
            usernameTextField.text = userName
        }
        
        if let email = UserDefaultManager.shared.currentUser?.email {
            emailTextField.text = email
        }
        
        if let userImage = UserDefaultManager.shared.currentUser?.photo {
            profileImage.loadImageFromUrl(imageUrl: userImage)
        }
    }
    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    func editProfile() {
        
        guard let username = usernameTextField.text, username.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }

        guard let myCurrentEmail = emailTextField.text, myCurrentEmail.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let password = oldPasswordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let newPassword = newPasswordTextField.text, newPassword.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور الجديدة"
            showError(error: apiError)
            return
        }
        
        guard let confirmpassword = reenterPasswordTextField.text, confirmpassword.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال تأكيد كلمة المرور الجديدة"
            showError(error: apiError)
            return
        }
        
        guard newPassword == confirmpassword else {
            let apiError = APIError()
            apiError.message = "كلمة المرور غير متطابقة"
            showError(error: apiError)
            return
        }
        
        
        if myImage?.jpegData(compressionQuality: 0.5) != nil {
            
            let imgData = myImage!.jpegData(compressionQuality: 0.5)
            print("imageData: \(imgData)")
            
            let parameters = [
                "name_user" : username as AnyObject,
                "user_pass_old" : password as AnyObject,
                "ID" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
                "user_pass" : newPassword as AnyObject,
            ]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfileWithImage(imageData: imgData!, basicDictionary: parameters, onSuccess: { (message) in
                
                self.stopLoadingWithSuccess()
                //                UserDefaultManager.shared.currentUser = newUser
                print(message)
                
                //                print("Photo: \(UserDefaultManager.shared.currentUser?.photo)")
                //                self.showDonePopUp()
                
                
            }) { (error) in
                print(error)
            }
            
        } else {
            
            let parameters = [
                "name_user" : username as AnyObject,
                "user_pass_old" : password as AnyObject,
                "ID" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
                "user_pass" : newPassword as AnyObject,
            ]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfile(basicDictionary: parameters, onSuccess: { (message) in
                
                self.stopLoadingWithSuccess()
                print(message)
                
                
            }) { (error) in
                print(error)
            }
            
        }
    
        
    }
    
    @IBAction func profileImageUploadIsPressed(_ sender: Any) {
        imagePressed()
    }
    
    @IBAction func editIsPressed(_ sender: Any) {
        print("Uploaded pressed")
        editProfile()
    }
    
}

extension AccountSettingsViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImage.image = image
            profileImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myImage = image
            self.dismiss(animated: true, completion: nil)
            
        }
    }
}
