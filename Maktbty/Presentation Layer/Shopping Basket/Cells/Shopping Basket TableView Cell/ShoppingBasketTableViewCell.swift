//
//  ShoppingBasketTableViewCell.swift
//  Maktbty
//
//  Created by Omar Ibrahim on 3/21/20.
//  Copyright © 2020 Gado. All rights reserved.
//

import UIKit

protocol BasketBookTableViewCellDeleteButtonDelegate {
    func didBasketButtonDeleteButtonPressed(id: Int)
}

class ShoppingBasketTableViewCell: UITableViewCell {
    
    var delegate: BasketBookTableViewCellDeleteButtonDelegate?
    
    //MARK:- Delegate Helpers
    func didBasketButtonDeleteButtonPressed(id: Int) {
        if let delegateValue = delegate {
            delegateValue.didBasketButtonDeleteButtonPressed(id: id)
        }
    }

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var bookRemoveButton: UIButton!
    @IBOutlet weak var bookAuthorNameLabel: UILabel!
    @IBOutlet weak var bookPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureData(basketBook: BasketBook) {
        if let image: String = basketBook.image {
            bookImageView.loadImageFromUrl(imageUrl: image)
        }
        
        if let bookTitle: String = basketBook.name {
            bookTitleLabel.text = bookTitle
        }
        
        if let bookAuthor: String = basketBook.authorName {
            bookAuthorNameLabel.text = bookAuthor
        }
        
        if let bookPrice: Int = basketBook.price, let bookPriceCurrency: String = basketBook.currencyName {
            bookPriceLabel.text = String(bookPrice) + " " + bookPriceCurrency
        }
        
        if let bookID: Int = basketBook.cartId {
            bookRemoveButton.tag = bookID
        }
    }
    
    @IBAction func bookRemoveFromBasketIsPressed(_ sender: Any) {
        print("Book is removed")
        self.didBasketButtonDeleteButtonPressed(id: bookRemoveButton.tag)
    }
    
}
