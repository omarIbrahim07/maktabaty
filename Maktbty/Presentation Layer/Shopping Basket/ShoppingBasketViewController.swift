//
//  ShoppingBasketViewController.swift
//  Maktbty
//
//  Created by Omar Ibrahim on 3/21/20.
//  Copyright © 2020 Gado. All rights reserved.
//

import UIKit

class ShoppingBasketViewController: BaseViewController {

    var paycontroller: PayFortController?

    var basketBooks: [BasketBook] = [BasketBook]()
    var memberId: Int?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var paymentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        getBasketBooks()
    }
    
    
    
    @IBAction func payButtonIsPressed(_ sender: Any) {
        print("Pay Button is pressed")
        self.getPaymentURL()
    }
        
    func pay() {
        let payFort = PayFortController(enviroment: KPayFortEnviromentSandBox)
        let request = NSMutableDictionary.init()
        request.setValue("1000", forKey: "amount")
        request.setValue("AUTHORIZATION", forKey: "command")
        
        request.setValue("USD", forKey: "currency")
        request.setValue("email@domain.com", forKey: "customer_email")
        request.setValue("en", forKey: "language")
        request.setValue("112233682686", forKey: "merchant_reference")
        request.setValue("token" , forKey: "sdk_token")

        payFort?.isShowResponsePage = true

        payFort?.callPayFort(withRequest: request, currentViewController: self, success: { (requestDic, responeDic) in
            print("success")
        }, canceled: { (requestDic, responeDic) in
            print("canceled")
        }, faild: { (requestDic, responeDic, message) in
            print("faild")
        })
    }
    
    // MARK:- Configuration
    func configureView() {
        paymentButton.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ShoppingBasketTableViewCell", bundle: nil), forCellReuseIdentifier: "ShoppingBasketTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Configuration
    func getBasketBooks() {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : self.memberId as AnyObject,
            "lang" : "ar" as AnyObject,
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().getBasketBooks(basicDictionary: parameters , onSuccess: { (basketBooks,total) in
            
            self.stopLoadingWithSuccess()
            
            self.basketBooks = basketBooks
            if self.basketBooks.count > 0 {
                print(total)
                self.paymentButton.isHidden = false
                self.paymentButton.setTitle("دفع \(total) ريال سعودي", for: .normal)
                self.tableView.reloadData()
            } else if self.basketBooks.count == 0 {
                self.paymentButton.isHidden = true
            }
            
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    func getPaymentURL() {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : self.memberId as AnyObject,
            "lang" : "ar" as AnyObject,
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().getPaymentURL(basicDictionary: parameters , onSuccess: { (paymentURL) in
            
            self.stopLoadingWithSuccess()
            print(paymentURL)
            self.goToPaymentWebView(paymentURL: paymentURL)
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    
    func deleteBasketBook(cartID: Int) {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : self.memberId as AnyObject,
            "lang" : "ar" as AnyObject,
            "cart_id" : cartID as AnyObject,
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().deleteBasketBook(basicDictionary: parameters , onSuccess: { (status) in
            
            self.stopLoadingWithSuccess()
            
            if status == true {
                self.getBasketBooks()
            } else if status == false {
                print("book isn't removed")
            }
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    // MARK:- Navigation
    func goToPaymentWebView(paymentURL: String) {
        let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        viewController.paymentURL = paymentURL
        //        viewController.storeId = self.storeId
        navigationController?.pushViewController(viewController, animated: true)
    }

}

//MARK:- TableView Delegate
extension ShoppingBasketViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.basketBooks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ShoppingBasketTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShoppingBasketTableViewCell", for: indexPath) as? ShoppingBasketTableViewCell {
            
            cell.delegate = self
            cell.configureData(basketBook: self.basketBooks[indexPath.row])
            
            return cell
        }
        
        return UITableViewCell()
    }
}

//MARK:- Delete Button delegate
extension ShoppingBasketViewController: BasketBookTableViewCellDeleteButtonDelegate {
    func didBasketButtonDeleteButtonPressed(id: Int) {
        self.deleteBasketBook(cartID: id)
    }
}
