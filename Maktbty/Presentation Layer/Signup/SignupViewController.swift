
//
//  SignupViewController.swift
//  Maktbty
//
//  Created by Gado on 5/21/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       configureView()
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    
  
    
    func configureView() {
        emailTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , borderWidth: 0.5)
        nameTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , borderWidth: 0.5)
        passwordTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , borderWidth: 0.5)
        mobileTextField.addCornerRadius(raduis: 19, borderColor:  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , borderWidth: 0.5)
        signupButton.addCornerRadius(raduis: 19, borderColor:  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , borderWidth: 0.5)
        
        navigationItem.title = "حساب جديد"
    }
    
    func goToLoginScreen()  {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "LogInViewController") as?  LogInViewController
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    func signup() {
        
        guard let mail = emailTextField.text, mail.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let name = nameTextField.text, name.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال الاسم"
            showError(error: apiError)
            return
        }
        
        guard let mobile = mobileTextField.text, mobile.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الهاتف"
            showError(error: apiError)
            return
        }
        
        guard let pass = passwordTextField.text, pass.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        
        
        
        print(pass.count)
        print(mail.count)
        
        let parameters  = [
            "email" : mail as AnyObject,
            "name_user" : name as AnyObject,
            "user_pass" : pass as AnyObject,
            "user_mobile" : mobile as AnyObject,
            
            ]
        
        weak var weakSelf = self
        
        startLoading()
        
        AuthenticationAPIManager().registerUser( basicDictionary: parameters, onSuccess: { (user) in
            
            self.stopLoadingWithSuccess()
            
            if user.status == true {
                let apiError = APIError()
                apiError.message = user.message
                self.goToLoginScreen()
                print("user: \(user)")
                return
            } else {
                let apiError = APIError()
                apiError.message = user.message
                self.showError(error: apiError)
            }
            
            
            //            self.packageId = packageIdd
            //            self.goToMap()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        signup()
    }

}
