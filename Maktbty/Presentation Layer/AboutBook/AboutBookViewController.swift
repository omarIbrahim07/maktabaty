//
//  AboutBookViewController.swift
//  Maktbty
//
//  Created by Gado on 6/11/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import Cosmos

class AboutBookViewController: BaseViewController {
    
    let reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
    
    var myBookDetails : BookDetails?
    var bookId : Int?
    
    var bookDetails: BookDetails?
    
    var mute: String?
    var play: String?
    var memberId: Int?
    
    @IBOutlet weak var viewContainingToolBar: UIView!
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var bookTitle: UIButton!
    @IBOutlet weak var authorName: UIButton!
    @IBOutlet weak var mainCategory: UIButton!
    @IBOutlet weak var price: UIButton!
    @IBOutlet weak var favoritesCount: UIButton!
    @IBOutlet weak var commentsCount: UIButton!
    @IBOutlet weak var averageRateCosmosView: CosmosView!
    @IBOutlet weak var playButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        getBookDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
    }
    
    func configureData()  {
        
        print(bookId)
        
        if let image = myBookDetails?.image {
            bookImageView.loadImageFromUrl(imageUrl: image)
        }
        
        bookTitle.setTitle(myBookDetails?.title, for: .normal)
        descriptionTextView.text = myBookDetails?.notes
        authorName.setTitle(myBookDetails?.author, for: .normal)
        mainCategory.setTitle(myBookDetails?.mainCategory, for: .normal)
        price.setTitle(String(myBookDetails!.price), for: .normal)
        favoritesCount.setTitle(String(myBookDetails!.heartsCount), for: .normal)
        commentsCount.setTitle(String(myBookDetails!.commentsCount), for: .normal)
        averageRateCosmosView.rating = Double(myBookDetails!.averageRate)
        averageRateCosmosView.text = String(myBookDetails!.averageRate)
    }
    
    func configureView() {
        
        bookImageView.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        descriptionTextView.flashScrollIndicators()
        
        //MARK: Reuse the view of the toolbar
//        let reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
        reusableView.frame = self.viewContainingToolBar.bounds
        self.viewContainingToolBar.addSubview(reusableView)
        
        if let book = bookDetails {
            reusableView.bookDetails = book
        }
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        reusableView.ButtonDelegate = self
        reusableView.BookButtonDelegate = self
        
        self.descriptionTextView.flashScrollIndicators()
        navigationItem.title = "عن الكتاب"
    }
    
    func goToMute(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Mute" {
            viewController.mute = "Mute"
        } else if key == "Unmute" {
            viewController.mute = "Unmute"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToPlay(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Play" {
            viewController.play = "Play"
        } else if key == "Pause" {
            viewController.play = "Pause"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getBookDetails() {
        
        print("BookId: \(bookId)")
        print("UserID: \(UserDefaultManager.shared.currentUser?.userID)")
        
        let parameters  = [
            "id" : bookId as AnyObject,
            "user_id" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
        ]
        
        startLoading()
        
        CategoriesAPIManager().getBookDetails(basicDictionary: parameters, onSuccess: { (BookDetails) in
            
            self.stopLoadingWithSuccess()
            
            print("BookDetails: \(BookDetails)")
            
            self.myBookDetails = BookDetails
            print(self.myBookDetails!)
            self.configureData()

            
        }) { (error) in
            print(error)
        }
    }
    
    func addBookToBasket(bookID: Int) {
        
        if let memId = UserDefaultManager.shared.currentUser?.userID {
            self.memberId = Int(memId)
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : self.memberId as AnyObject,
            "lang" : "ar" as AnyObject,
            "book_id" : bookID as AnyObject,
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().addBookToBasket(basicDictionary: parameters , onSuccess: { (status) in
            
            self.stopLoadingWithSuccess()
            
            if status == true {
//                self.getBasketBooks()
                print("Book Added")
            } else if status == false {
                print("book isn't removed")
            }
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    
    func shareBook(shareLink: String) {
        
        if let name = NSURL(string: shareLink) {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
        else
        {
            // show alert for not available
        }
    }
    
    //MARK: start the app with the textview scrolling on top
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        descriptionTextView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func goToHome() {
        if let evaluateNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationHomeViewController") as? UINavigationController, let rootViewContoller = evaluateNavigationController.viewControllers[0] as? HomeViewController {
            
            if let book = self.bookDetails {
                rootViewContoller.bookDetails = book
            }
            
            UIApplication.shared.keyWindow?.rootViewController = evaluateNavigationController
            
            print("Fes")
        }
    }
    
    func goToAuthors() {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AuthorsViewController") as?  AuthorsViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }


    @IBAction func playButtonPressed(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "SoundScreenViewController") as?  SoundScreenViewController
        
        guard let bookDetailsObject = myBookDetails else {  return }
        destinationVC!.soundScreenViewControllerBookDetails = bookDetailsObject
        
        guard let image = bookImageView.image else {  return }
        destinationVC!.bookImage = image
        
        self.present(destinationVC!, animated: true)
    }
    
    @IBAction func authorNameButtonClicked(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AboutAuthorViewController") as?  AboutAuthorViewController
        guard let id = myBookDetails?.autherId else {  return }
        destinationVC!.authorId = id
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    @IBAction func mainCategoryButtonClicked(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "GenericBooksScreensViewController") as?  GenericBooksScreensViewController
        guard let id = myBookDetails!.mainCategoryId else {  return }
        guard let name = myBookDetails!.mainCategory else {  return }

        destinationVC!.categoryId = id
        destinationVC!.flag = 4
        destinationVC!.categoryName = name
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    @IBAction func commentsCountClicked(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "CommentsPageViewController") as?  CommentsPageViewController
        guard let id = myBookDetails!.id else {  return }
        
        destinationVC!.bookId = id
        destinationVC!.flag = 2
        self.navigationController?.pushViewController(destinationVC!, animated: true)
//        self.present(destinationVC!, animated: true, completion: nil)
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        if let link = myBookDetails?.shareLink {
            shareBook(shareLink: link)
        }
    }
    
    @IBAction func addBookToBasketButtonIsPressed(_ sender: Any) {
        print("Price is pressed")
        if let bookID: Int = myBookDetails?.id {
            self.addBookToBasket(bookID: bookID)
        }
    }
    

}

extension AboutBookViewController: ViewButtonDelegate, ViewSoundDelegate {
    
    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails) {
        if keyWord == "Mute" {
            goToMute(key: "Mute", bookDetailss: bookDetils)
        } else if keyWord == "Unmute" {
            goToMute(key: "Unmute", bookDetailss: bookDetils)
        } else if keyWord == "Play" {
            goToPlay(key: "Play", bookDetailss: bookDetils)
        } else if keyWord == "Pause" {
            goToPlay(key: "Pause", bookDetailss: bookDetils)
        }
    }
    
    
    func didButtonPressed(keyWord: String) {
        if keyWord == "Author" {
            self.goToAuthors()
        } else if keyWord == "Favourite" {
            self.goToFavourites()
        } else if keyWord == "Home" {
            self.goToHome()
        }
    }
    
}
