//
//  BookPartsViewController.swift
//  Maktbty
//
//  Created by Gado on 6/19/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

//protocol ButtonPartTableViewCellDelegate {
//    func didButtonPartPressed(tag: Int)
//}

class BookPartsViewController: BaseViewController  {

    var bookPartsViewControllerBookDetails : BookDetails?
    var PartButtonDelegate: ButtonPartTableViewCellDelegate?
    
    @IBOutlet weak var bookPartsTableView: UITableView!
    @IBOutlet weak var dismissButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableview()
    }
 
    
    func configureTableview()  {
        self.bookPartsTableView.rowHeight = 95
        
        // Do any additional setup after loading the view.
        bookPartsTableView.delegate = self
        bookPartsTableView.dataSource = self
        bookPartsTableView.register(UINib(nibName: "BookPartsTableViewCell", bundle: nil) , forCellReuseIdentifier: "BookPartsTableViewCell")
    }
    
    //MARK:- Delegate Helpers
    func didButtonPartPressed(tag: Int) {
        if let delegateValue = PartButtonDelegate {
            delegateValue.didButtonPartPressed(tag: tag)
        }
    }
    

    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension BookPartsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookPartsViewControllerBookDetails?.audio.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bookPartsTableView.dequeueReusableCell(withIdentifier: "BookPartsTableViewCell") as! BookPartsTableViewCell
        cell.bookPartNameLabel.text = bookPartsViewControllerBookDetails?.title
        cell.bookPartImage.loadImageFromUrl(imageUrl: bookPartsViewControllerBookDetails!.image)
        cell.bookPartNumberLabel.text = String(indexPath.row + 1)
        cell.playButton.tag = indexPath.row
        cell.BookPartButtonDelegate = self
        return cell
    }
    
}

extension BookPartsViewController: BookButtonPartTableViewCellDelegate {
    
    func didBookButtonPartPressed(tag: Int) {
        print("7obby")
        self.dismiss(animated: true, completion: nil)
        self.PartButtonDelegate?.didButtonPartPressed(tag: tag)
    }
}
