//
//  BookPartsTableViewCell.swift
//  Maktbty
//
//  Created by Gado on 6/19/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

protocol BookButtonPartTableViewCellDelegate {
    func didBookButtonPartPressed(tag: Int)
}

class BookPartsTableViewCell: UITableViewCell {
    
    var BookPartButtonDelegate: BookButtonPartTableViewCellDelegate?
    
    @IBOutlet weak var bookPartNameLabel: UILabel!
    @IBOutlet weak var bookPartNumberLabel: UILabel!
    @IBOutlet weak var bookPartImage: UIImageView!
    @IBOutlet weak var bookPartFrameImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        
        
    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
 
    func configureView()  {
        
        bookPartImage.layer.borderWidth = 1
        bookPartImage.layer.masksToBounds = false
        bookPartImage.layer.borderColor = UIColor.black.cgColor
        bookPartImage.layer.cornerRadius = bookPartImage.frame.height/2
        bookPartImage.clipsToBounds = true
        
        bookPartNumberLabel.layer.borderWidth = 1
        bookPartNumberLabel.layer.masksToBounds = false
        bookPartNumberLabel.layer.borderColor = UIColor.black.cgColor
        bookPartNumberLabel.layer.cornerRadius = bookPartNumberLabel.frame.height/2
        bookPartNumberLabel.clipsToBounds = true
    }
    
    //MARK:- Delegate Helpers
    func didBookButtonPartPressed(tag: Int) {
        if let delegateValue = BookPartButtonDelegate {
            delegateValue.didBookButtonPartPressed(tag: tag)
        }
    }
    
    var isPlaying = false

    

    @IBAction func playButtontabbed(_ sender: Any) {
        if isPlaying
        {
            setOnPause()
        } else {
            setOnPlay()
            didBookButtonPartPressed(tag: playButton.tag)
        }
    }
    
    func setOnPause()
    {
        
        isPlaying = false
        
        let myImage = UIImage(named: "play-button-1")
        playButton.setImage( myImage, for: .normal)
        
    }
    
    func setOnPlay()
    {
        
        isPlaying = true
        
        let myImage = UIImage(named: "pause (1)")
        playButton.setImage( myImage, for: .normal)
    }
    
    
    
}
