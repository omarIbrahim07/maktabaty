//
//  AuthorsViewController.swift
//  Maktbty
//
//  Created by Gado on 6/13/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class AuthorsViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
    
    var myAuthors = [Authors]()
    var searchedString: String?
    
    var searchedAuthors: [Authors] = []
    
    var bookDetails: BookDetails?
    
    var mute: String?
    var play: String?
    
    @IBOutlet weak var authorsCollectionView: UICollectionView!
    @IBOutlet weak var viewContainingToolBar: UIView!
    @IBOutlet weak var authorsButton: UIButton!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var authorsSearchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getAuthors()
        
        authorsSearchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
    }
    
    func configureView()  {
        //MARK: Reuse the view of the toolbar
//        let reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
        reusableView.frame = self.viewContainingToolBar.bounds
        self.viewContainingToolBar.addSubview(reusableView)
        
        if let book = bookDetails {
            reusableView.bookDetails = book
        }
        
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        reusableView.ButtonDelegate = self
        reusableView.BookButtonDelegate = self
        
        self.authorsCollectionView.delegate = self
        self.authorsCollectionView.dataSource = self
        self.authorsCollectionView.register(UINib(nibName: "AuthorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AuthorCollectionViewCell")
        
        categoriesButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        authorsButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        navigationItem.title = "المؤلفون"
    }
    
    func getAuthors() {
        
        startLoading()
        
        CategoriesAPIManager().getAuthors(basicDictionary: [:], onSuccess: { (Authors) in
            
            self.stopLoadingWithSuccess()
            self.myAuthors = Authors
            print(self.myAuthors)
            self.authorsCollectionView.reloadData()
        }) { (error) in
            print(error)
        }
        
    }
    
    func getSearchedAuthors() {
        
        guard let searchedString = self.searchedString else{
            return
        }
        
        let parameters: [String:String] = ["title_auther" : searchedString]
        
        self.startLoading()
        
        CategoriesAPIManager().searchForAuthors(basicDictionary: parameters as [String : AnyObject], onSuccess: { (searchedAuthors) in
            
            self.stopLoadingWithSuccess()
            
            self.searchedAuthors = searchedAuthors
            
            self.goToSearchingAdvertisements()
            
        }) { (error) in
            print("error")
        }
    }
    
    func goToSearchingAdvertisements() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        viewController.searchedAuthors = self.searchedAuthors
        viewController.searchedType = "Author"
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMute(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Mute" {
            viewController.mute = "Mute"
        } else if key == "Unmute" {
            viewController.mute = "Unmute"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToPlay(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Play" {
            viewController.play = "Play"
        } else if key == "Pause" {
            viewController.play = "Pause"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToHome() {
        if let evaluateNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationHomeViewController") as? UINavigationController, let rootViewContoller = evaluateNavigationController.viewControllers[0] as? HomeViewController {
            
            if let book = self.bookDetails {
                rootViewContoller.bookDetails = book
            }
            
            UIApplication.shared.keyWindow?.rootViewController = evaluateNavigationController
            
            print("Fes")
        }
    }
    
    @IBAction func categoriesButtonClicked(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "CategoriesViewController") as?  CategoriesViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }

}

extension AuthorsViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myAuthors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = authorsCollectionView.dequeueReusableCell(withReuseIdentifier: "AuthorCollectionViewCell", for: indexPath) as! AuthorCollectionViewCell
        cell.setData(text: myAuthors[indexPath.row].name)
        cell.authorImage.loadImageFromUrl(imageUrl: myAuthors[indexPath.row].image)
        cell.averageRateCosmosView.text = String(myAuthors[indexPath.row].averageRate)
        cell.averageRateCosmosView.rating = Double(myAuthors[indexPath.row].averageRate)

        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AboutAuthorViewController") as?  AboutAuthorViewController
        guard let id = myAuthors[indexPath.row].id else {  return }
        destinationVC!.authorId = id
        self.navigationController?.pushViewController(destinationVC!, animated: true)
        
    }
    
}

extension AuthorsViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 40 )/3.0
        return CGSize (width: width, height: width * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 10 , left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
//    func calculateWith() -> CGFloat {
//        let estimatedWidth = CGFloat(estimateWidth)
//        let cellcount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
//        let margin = CGFloat(cellMarginSize * 2)
//        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellcount - 1) - margin) / cellcount
//
//        return width
//    }
    
    
}

extension AuthorsViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            // here is text from the search bar
            print(text)
            self.searchedString = text
            
            //            userInput = text
            // u must pass text here to searching viewcontroller ,, so u must make goToSerching take the text there
            self.getSearchedAuthors()
        }
    }
}

extension AuthorsViewController: ViewButtonDelegate, ViewSoundDelegate {
    
    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails) {
        if keyWord == "Mute" {
            goToMute(key: "Mute", bookDetailss: bookDetils)
        } else if keyWord == "Unmute" {
            goToMute(key: "Unmute", bookDetailss: bookDetils)
        } else if keyWord == "Play" {
            goToPlay(key: "Play", bookDetailss: bookDetils)
        } else if keyWord == "Pause" {
            goToPlay(key: "Pause", bookDetailss: bookDetils)
        }
    }
    
    
    func didButtonPressed(keyWord: String) {
        if keyWord == "Author" {
            
        } else if keyWord == "Favourite" {
            self.goToFavourites()
        } else if keyWord == "Home" {
            goToHome()
        }
    }
    
}

