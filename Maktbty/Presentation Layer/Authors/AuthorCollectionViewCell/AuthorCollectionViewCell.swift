//
//  AuthorCollectionViewCell.swift
//  Maktbty
//
//  Created by Gado on 6/13/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import Cosmos

class AuthorCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        authorName.addCornerRadius(raduis: 10, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        authorImage.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)

    }
    
    func setData(text: String)  {
        self.authorName.setTitle(text, for: .normal)
    }

    @IBOutlet weak var authorName: UIButton!
    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var averageRateCosmosView: CosmosView!
    
}
