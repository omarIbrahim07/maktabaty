//
//  LogInViewController.swift
//  Maktbty
//
//  Created by Gado on 5/20/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class LogInViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()

        // Do any additional setup after loading the view.
    }
    
    
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        logIn()
    }
    @IBOutlet weak var createAccount: UIButton!
    
    @IBAction func createAccount(_ sender: Any) {
        
    }
    
    func logIn() {

        guard let mail = mailTextField.text, mail.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let pass = passwordTextfield.text, pass.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        print(pass.count)
        print(mail.count)

        let parameters  = [
            "email" : mail as AnyObject,
            "user_pass" : pass as AnyObject,
        ]

        weak var weakSelf = self

        startLoading()

        AuthenticationAPIManager().login( basicDictionary: parameters, onSuccess: { (id) in

            self.stopLoadingWithSuccess()
            
            self.getUserProfile(id: Int(id)!)

        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }

    }
    
    func getUserProfile(id: Int) {
        
        let parameters  = [
            "ID" : id as AnyObject,
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        AuthenticationAPIManager().getUserProfile(basicDictionary: parameters, onSuccess: { (user) in
            
            self.stopLoadingWithSuccess()
            
            if user.name?.count ?? 0 > 0 {
                self.goToHome()
                print("user: \(user)")
                return
            } else {
                let apiError = APIError()
                apiError.message = "خطأ في البريد الالكتروني او كلمة المرور"
                self.showError(error: apiError)
            }
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func goToHome()  {
        
        if let _ = UserDefaultManager.shared.currentUser {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationHomeViewController")
            appDelegate.window!.rootViewController = viewController
            appDelegate.window!.makeKeyAndVisible()
        }
//        let sb = UIStoryboard.init(name: "Main", bundle: nil)
//        let destinationVC = sb.instantiateViewController(
//            withIdentifier: "HomeViewController") as?  HomeViewController
//        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }

    func configureView()  {
        mailTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), borderWidth: 0.5)
        passwordTextfield.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), borderWidth: 0.5)
        loginButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), borderWidth: 0.5)
        createAccount.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), borderWidth: 0.5)

    }

}
