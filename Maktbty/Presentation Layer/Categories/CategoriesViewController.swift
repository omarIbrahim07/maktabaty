//
//  ViewController.swift
//  Maktbty
//
//  Created by Gado on 6/9/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class CategoriesViewController: BaseViewController {
  
    var reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
    
    var myCategories: [Category] = []
    
    var bookDetails: BookDetails?
    
    var mute: String?
    var play: String?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var authorsButton: UIButton!
    @IBOutlet weak var enjoyDownloadingLabel: UILabel!
    @IBOutlet weak var subscripeButton: UIButton!
    @IBOutlet weak var subscriptionBooksButton: UIButton!
    @IBOutlet weak var saleBooksButton: UIButton!
    @IBOutlet weak var freeBooksButton: UIButton!
    @IBOutlet weak var viewContainToolBar: UIView!

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTableView()
        getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        
    }
    
    func doSegue(flag : Int){
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "GenericBooksScreensViewController") as?  GenericBooksScreensViewController
        destinationVC!.flag = flag
        self.navigationController?.pushViewController(destinationVC!, animated: true)
        //        self.present(destinationVC!, animated: true, completion: nil)
    }
    

    
    
    
    
    
    func configureView() {
        categoriesButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        authorsButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        enjoyDownloadingLabel.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        subscripeButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        subscriptionBooksButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        saleBooksButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        freeBooksButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        
        //MARK: Reuse the view of the toolbar
//        let reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        
        if let book = bookDetails {
            reusableView.bookDetails = book
        }
        
        print("Fes")
        
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")

        
        reusableView.ButtonDelegate = self
        reusableView.BookButtonDelegate = self
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        
        navigationItem.title = "الأقسام"
    }
    
    func goToMute(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Mute" {
            viewController.mute = "Mute"
        } else if key == "Unmute" {
            viewController.mute = "Unmute"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToPlay(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Play" {
            viewController.play = "Play"
        } else if key == "Pause" {
            viewController.play = "Pause"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func configureTableView() {
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.isEditing = false
        tableView.register(UINib(nibName: "CategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoriesTableViewCell")
        
        self.tableView.rowHeight = 50
    }
    
    func getCategories() {
        
        startLoading()
        
        CategoriesAPIManager().getCategories(basicDictionary: [:], onSuccess: { (categories) in
            
            self.stopLoadingWithSuccess()
            
            self.myCategories = categories
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
        
    }
    
    func goToAuthors() {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AuthorsViewController") as?  AuthorsViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    func goToHome() {
        if let evaluateNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationHomeViewController") as? UINavigationController, let rootViewContoller = evaluateNavigationController.viewControllers[0] as? HomeViewController {
            
            if let book = self.bookDetails {
                rootViewContoller.bookDetails = book
            }
            
            UIApplication.shared.keyWindow?.rootViewController = evaluateNavigationController
            
            print("Fes")
        }
    }

    
    @IBAction func subscribeButtonIsPressed(_ sender: Any) {
        
        if let mySubscriptionsNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionsNavigationController") as? UINavigationController, let _ = mySubscriptionsNavigationController.viewControllers[0] as? SubscriptionsViewController {
            self.present(mySubscriptionsNavigationController, animated: true, completion: nil)
        }
        
        
        
    }
    
    @IBAction func authorsButtonIsPressed(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AuthorsViewController") as?  AuthorsViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    @IBAction func freeBooksButtonClicked(_ sender: Any) {
        doSegue(flag: 1)
    }
    @IBAction func saleBooksButtonClicked(_ sender: Any) {
        doSegue(flag: 2)
    }
    @IBAction func subscribtionBooksClicked(_ sender: Any) {
        doSegue(flag: 3)
    }
    
    
}

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell")! as! CategoriesTableViewCell
        cell.categoryButton.setTitle(myCategories[indexPath.row].name, for: .normal)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "GenericBooksScreensViewController") as?  GenericBooksScreensViewController
        guard let id = myCategories[indexPath.row].id else {  return }
        guard let name = myCategories[indexPath.row].name else {  return }

        destinationVC!.categoryId = id
        destinationVC!.categoryName = name
        destinationVC!.flag = 4
        self.navigationController?.pushViewController(destinationVC!, animated: true)
//        self.present(destinationVC!, animated: true, completion: nil)

    }
    
}

extension CategoriesViewController: ViewButtonDelegate, ViewSoundDelegate {
    
    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails) {
        if keyWord == "Mute" {
            goToMute(key: "Mute", bookDetailss: bookDetils)
        } else if keyWord == "Unmute" {
            goToMute(key: "Unmute", bookDetailss: bookDetils)
        } else if keyWord == "Play" {
            goToPlay(key: "Play", bookDetailss: bookDetils)
        } else if keyWord == "Pause" {
            goToPlay(key: "Pause", bookDetailss: bookDetils)
        }
    }
    
    
    func didButtonPressed(keyWord: String) {
        if keyWord == "Author" {
            self.goToAuthors()
        } else if keyWord == "Favourite" {
            self.goToFavourites()
        } else if keyWord == "Home" {
            self.goToHome()
        }
    }
    
}


