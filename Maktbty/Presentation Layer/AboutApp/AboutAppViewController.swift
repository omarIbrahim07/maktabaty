//
//  AboutAppViewController.swift
//  Maktbty
//
//  Created by Gado on 6/24/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit


class AboutAppViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


    @IBAction func aboutAppButtonTapped(_ sender: Any) {
        
        guard let url = URL(string: "http://demo.maktbti.com/about.php") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func policyButtonTapped(_ sender: Any) {
        
        guard let url = URL(string: "http://demo.maktbti.com/privacy.php") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    @IBAction func termsConditionsButtonTapped(_ sender: Any) {
        
        guard let url = URL(string: "http://demo.maktbti.com/terms.php") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }

}
