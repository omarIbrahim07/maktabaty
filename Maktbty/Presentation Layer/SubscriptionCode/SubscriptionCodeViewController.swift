//
//  SubscriptionCodeViewController.swift
//  Maktbty
//
//  Created by Gado on 5/21/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class SubscriptionCodeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        subscriptionCodeTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        cancelButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        sendButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), borderWidth: 0.5)

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var subscriptionCodeTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
