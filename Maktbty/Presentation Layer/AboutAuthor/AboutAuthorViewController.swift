//
//  AboutAuthorViewController.swift
//  Maktbty
//
//  Created by Gado on 5/30/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import Cosmos

class AboutAuthorViewController: BaseViewController {

    var myAuthorDetails : AuthorDetails?
    var authorId = 0
    
    @IBOutlet weak var authorNameButton: UIButton!
    @IBOutlet weak var evaluationsVeiw: UIView!
    @IBOutlet weak var revisionsView: UIView!
    @IBOutlet weak var readsView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var commentsCountButton: UIButton!
    @IBOutlet weak var averageRateCosmosView: CosmosView!
    @IBOutlet weak var evaluationsCountLabel: UILabel!
    @IBOutlet weak var revisionsCountLabel: UILabel!
    @IBOutlet weak var readsCountLabel: UILabel!
    @IBOutlet weak var authorImage: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getAuthorDetails()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        
        authorNameButton.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        evaluationsVeiw.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        revisionsView.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        readsView.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        authorImage.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        descriptionTextView.flashScrollIndicators()
        navigationItem.title = "عن المؤلف"
    }
    
    
    func getAuthorDetails() {
        
        startLoading()
        
        CategoriesAPIManager().getAuthorDetails(authorId: authorId, basicDictionary: [:], onSuccess: { (authorDetails) in
            
            self.stopLoadingWithSuccess()
            self.myAuthorDetails = authorDetails
            print(self.myAuthorDetails!)
            self.authorNameButton.setTitle(self.myAuthorDetails?.name, for: .normal)
            self.evaluationsCountLabel.text = String(self.myAuthorDetails!.evaluation)
            self.readsCountLabel.text = String(self.myAuthorDetails!.readersCount)
            self.revisionsCountLabel.text = String(self.myAuthorDetails!.revisionsCount)
            self.descriptionTextView.text = self.myAuthorDetails!.notes
            self.commentsCountButton.setTitle(String(self.myAuthorDetails!.commentsCount), for: .normal)
            self.averageRateCosmosView.text = String(self.myAuthorDetails!.averageRate)
            self.averageRateCosmosView.rating = Double(self.myAuthorDetails!.averageRate)
            self.authorImage.loadImageFromUrl(imageUrl: self.myAuthorDetails!.image)
        }) { (error) in
            print(error)
        }
        
    }
    
    
    //MARK: start the app with the textview on top
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        descriptionTextView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func shareAuthor(shareLink: String) {
        
        if let name = NSURL(string: shareLink) {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
        else
        {
            // show alert for not available
        }
        
    }
    

    @IBAction func commentsCountButtonClicked(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "CommentsPageViewController") as?  CommentsPageViewController
        guard let id = myAuthorDetails!.id else {  return }
        
        destinationVC!.authorId = id
        destinationVC!.flag = 1
        self.navigationController?.pushViewController(destinationVC!, animated: true)
//        self.present(destinationVC!, animated: true, completion: nil)

    }
    
    @IBAction func shareAuthorIsPressed(_ sender: Any) {
        
        print("Share pressed")
        
        if let link = myAuthorDetails?.shareLink {
            shareAuthor(shareLink: link)
        }
        
    }
    
    
}
