//
//  WebViewViewController.swift
//  Maktbty
//
//  Created by Omar Ibrahim on 3/26/20.
//  Copyright © 2020 Gado. All rights reserved.
//

import UIKit

class WebViewViewController: BaseViewController {

    var paymentURL: String?
    
    @IBOutlet weak var paymentWebsiteWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let paymentURL = self.paymentURL {
            configureWebView(paymentURL: paymentURL)
        }
    }
    
    func configureWebView(paymentURL: String) {
        paymentWebsiteWebView.loadRequest(URLRequest(url: URL(string: paymentURL)!))
        navigationItem.title = "الدفع الإلكتروني"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
