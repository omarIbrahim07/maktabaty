//
//  HomeViewController.swift
//  Maktbty
//
//  Created by Gado on 6/27/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import FSPagerView
import SideMenu

protocol ChildViewControllerDelegate
{
    func childViewControllerResponse(mute: String, play: String, index: Int)
}

class HomeViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
    
    var myBooks: [BooksByCategory] = []
    var flag : Int = 3
    
    var searchedBooks: [SearchedBook] = []
    var searchedString: String?
    var myHomeSlider = [HomeSlider]()
    
    var bookDetails: BookDetails?
    var delegate: ChildViewControllerDelegate?

    var mute: String?
    var play: String?
    
    @IBOutlet weak var bookSearchBar: UISearchBar!
    @IBOutlet weak var subscribtionBookButton: UIButton!
    @IBOutlet weak var freeBooksButton: UIButton!
    @IBOutlet weak var saleBooksButton: UIButton!
    @IBOutlet weak var authorsButton: UIButton!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        configureUICollectionView()
        configureFSPagerView()
        getHomeSlider()
        selectCollectionType(flag: flag)
        configureNavigationBar()
        configureSideMenu()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("&000000000000")
        
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        
    }
    
    //MARK:- API Calls
    
    
    //MARK:- sideMenu
    func configureSideMenu() {
        
        
        // Define the menus
        let rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightSideMenu") as? UISideMenuNavigationController
//        SideMenuManager.default.rightMenuNavigationController = sideMenuObj
        
//        let rightMenuNavigationController = UISideMenuNavigationController(rootViewController: sideMenuObj!)
        SideMenuManager.default.rightMenuNavigationController = rightMenuNavigationController
        
        // Setup gestures: the left and/or right menus must be set up (above) for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        //        SideMenuManager.default.addPanGestureToPresent(toView: self.navigationController!.navigationBar)
        //        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        if let x = self.navigationController
        {
        
        SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.right)
        }
        
        

        // (Optional) Prevent status bar area from turning black when menu appears:
        rightMenuNavigationController!.statusBarEndAlpha = 0

        // Copy all settings to the other menu
        //        rightMenuNavigationController.settings = leftMenuNavigationController.settings
        
    }
    
    
    //MARK:- NavigationBar
    override func configureNavigationBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
//        self.navigationController?.view.semanticContentAttribute = .forceLeftToRight
        
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "ic_drawer"), for: .normal)
        //        button.setTitle("YourTitle", for: .normal)
        button.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        //        button.frame = CGRect(x: 10, y: 10, width: 10, height: 5)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        button.addTarget(self, action: #selector(self.pressed(sender:)), for: .touchUpInside)
        bookSearchBar.delegate = self
    }
    
    @objc func pressed(sender: UIButton!) {
        // Define the menu
        //        let menu = UISideMenuNavigationController(rootViewController: YourViewController)
        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        let menu = storyboard!.instantiateViewController(withIdentifier: "RightSideMenu") as! UISideMenuNavigationController
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    
    
    
    
    func selectCollectionType(flag : Int){
        
        switch flag {
        case 1:
            freeBooksButton.backgroundColor = #colorLiteral(red: 0.3452302814, green: 0.3488911688, blue: 0.3576574922, alpha: 1)
            freeBooksButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            saleBooksButton.backgroundColor = mainColor
            saleBooksButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            subscribtionBookButton.backgroundColor = mainColor
            subscribtionBookButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            getFreeSubscribtionSaleBooks()
            
            
            
        case 2:
            
            freeBooksButton.backgroundColor = mainColor
            freeBooksButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            saleBooksButton.backgroundColor = #colorLiteral(red: 0.3452302814, green: 0.3488911688, blue: 0.3576574922, alpha: 1)
            saleBooksButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            subscribtionBookButton.backgroundColor = mainColor
            subscribtionBookButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            getFreeSubscribtionSaleBooks()
            
        case 3:
            
            freeBooksButton.backgroundColor = mainColor
            freeBooksButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            saleBooksButton.backgroundColor = mainColor
            saleBooksButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            subscribtionBookButton.backgroundColor = #colorLiteral(red: 0.3452302814, green: 0.3488911688, blue: 0.3576574922, alpha: 1)
            subscribtionBookButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            getFreeSubscribtionSaleBooks()
        default:
            return
        }
        

        
        
//        let sb = UIStoryboard.init(name: "Main", bundle: nil)
//        let destinationVC = sb.instantiateViewController(
//            withIdentifier: "GenericBooksScreensViewController") as?  GenericBooksScreensViewController
//        destinationVC!.flag = flag
//        self.navigationController?.pushViewController(destinationVC!, animated: true)
//        //        self.present(destinationVC!, animated: true, completion: nil)
    }
    
    func configureView() {
        
//        var reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        
        if let book = bookDetails {
            reusableView.bookDetails = book
        }
        
        play = UserDefaults.standard.string(forKey: "play")
        mute = UserDefaults.standard.string(forKey: "mute")
        
        print("Fes")
        
        reusableView.ButtonDelegate = self
        reusableView.BookButtonDelegate = self
        
        if let playCheking = play {
            reusableView.play = playCheking
            
            if reusableView.play == "Played" {
                reusableView.isPlaying = true
                let myImage = UIImage(named: "pause5")
                reusableView.playButton.image = myImage
            } else if reusableView.play == "Paused" {
                reusableView.isPlaying = false
                let myImage = UIImage(named: "play-button (4)")
                reusableView.playButton.image = myImage
            }
        }
        
        if let muteChecking = mute {
            reusableView.mute = muteChecking
            
            if reusableView.mute == "Muted" {
                reusableView.isMuting = true
                let myImage = UIImage(named: "01_0001_Rectangle-1")
                reusableView.muteButton.image = myImage
            } else if reusableView.mute == "Unmuted" {
                reusableView.isMuting = false
                let myImage = UIImage(named: "sma3a")
                reusableView.muteButton.image = myImage
            }
        }
        
        categoriesButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        authorsButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        subscribtionBookButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        freeBooksButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        saleBooksButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        pagerView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        self.title = "الرئيسية"

        
    }
    
    func configureUICollectionView() {
        
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        homeCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
    }
    
    func configureFSPagerView() {
        
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.transformer = FSPagerViewTransformer(type: .depth)
    }
    
    // MARK:- API Configuration
    func getHomeSlider() {
        
        startLoading()
        
        CategoriesAPIManager().getHomeSlider(basicDictionary: [:], onSuccess: { (homeSlider) in
            
            self.stopLoadingWithSuccess()
            
            self.myHomeSlider = homeSlider
            self.pagerView.reloadData()
        }) { (error) in
            print(error)
        }
        
    }
    
    func getFreeSubscribtionSaleBooks() {
        
        startLoading()
        
        CategoriesAPIManager().getFreeSubscribtionSaleBooks(userId: 1, typeId: flag, basicDictionary: [:], onSuccess: { (books) in
            
            self.stopLoadingWithSuccess()
            self.myBooks = books
            print(self.myBooks)
            self.homeCollectionView.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func getSearchedBooks() {
        
        guard let searchedString = self.searchedString else{
            return
        }
        
        let parameters: [String:String] = ["title_book" : searchedString]
        
        self.startLoading()
        
        CategoriesAPIManager().searchForBooks(basicDictionary: parameters as [String : AnyObject], onSuccess: { (searchedBookssss) in
            
            self.stopLoadingWithSuccess()

            self.searchedBooks = searchedBookssss
                        
            self.goToSearchingAdvertisements()
            
        }) { (error) in
            print("error")
        }
    }
    
    // MARK:- Navigation
    func goToSearchingAdvertisements() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        viewController.searchedBooks = self.searchedBooks
        viewController.searchedType = "Home"
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMute(key: String, bookDetailss: BookDetails) {
    
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Mute" {
            viewController.mute = "Mute"
        } else if key == "Unmute" {
            viewController.mute = "Unmute"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToPlay(key: String, bookDetailss: BookDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SoundScreenViewController") as! SoundScreenViewController
        if key == "Play" {
            viewController.play = "Play"
        } else if key == "Pause" {
            viewController.play = "Pause"
        }
        viewController.playAgain = "P"
        viewController.soundScreenViewControllerBookDetails = bookDetailss
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToAuthors() {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AuthorsViewController") as?  AuthorsViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    func goToCategories() {
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "CategoriesViewController") as?  CategoriesViewController
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    func goToBasketBooks() {
        if let basketBooksNavigationViewController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "ShoppingBasketNavigationViewController") as? UINavigationController, let _ = basketBooksNavigationViewController.viewControllers[0] as? ShoppingBasketViewController {
            self.present(basketBooksNavigationViewController, animated: true, completion: nil)
        }
    }

    
    // MARK:- Actions
    @IBAction func categoriesButtonClicked(_ sender: Any) {
        goToCategories()
    }
    
    @IBAction func authorsButtonClicked(_ sender: Any) {
        goToAuthors()
    }
    
    @IBAction func freeBooksButtonClicked(_ sender: Any) {
        flag = 1
        selectCollectionType(flag: flag)
        
    }
    @IBAction func saleBooksButtonClicked(_ sender: Any) {
        flag = 2
        selectCollectionType(flag: flag)
        
    }
    @IBAction func subscribtionBookClicked(_ sender: Any) {
        flag = 3
        selectCollectionType(flag: flag)
        
    }
    
    @IBAction func basketBooksButtonIsPressed(_ sender: Any) {
        print("Basket Button is pressed")
        self.goToBasketBooks()
    }
    

}

extension HomeViewController :  FSPagerViewDelegate, FSPagerViewDataSource  {
    
    
    
    // MARK: Implement FSPagerViewDataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return myHomeSlider.count
//        return numberOfItems.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.loadImageFromUrl(imageUrl: myHomeSlider[index].image)
        //        cell.imageView?.image = UIImage(named: "hugo")
//        cell.textLabel?.text = myHomeSlider[index].title

        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AboutBookViewController") as?  AboutBookViewController
        guard let id = myHomeSlider[index].bookId else {  return }
        destinationVC!.bookId = id
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
//        self.present(destinationVC!, animated: true)

    }
    
    
}


extension HomeViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myBooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.setData(text: myBooks[indexPath.row].title)
        cell.bookImage.loadImageFromUrl(imageUrl: myBooks[indexPath.row].image)
        cell.commentsCountButton.setTitle(String(myBooks[indexPath.row].commentsCount), for: .normal)
        cell.favoritesCountButton.setTitle(String(myBooks[indexPath.row].heartsCount), for: .normal)
        cell.averageRateCosmosView.text = String(myBooks[indexPath.row].averageRate)
        cell.averageRateCosmosView.rating = Double(myBooks[indexPath.row].averageRate)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AboutBookViewController") as?  AboutBookViewController
        guard let id = myBooks[indexPath.row].id else {  return }
        destinationVC!.bookId = id
        
        if let book = self.bookDetails {
            destinationVC?.bookDetails = book
            destinationVC?.mute = mute
            destinationVC?.play = play
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
}

extension HomeViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            // here is text from the search bar
            print(text)
            self.searchedString = text
            
            //            userInput = text
            // u must pass text here to searching viewcontroller ,, so u must make goToSerching take the text there
            self.getSearchedBooks()
        }
    }
}

extension HomeViewController: ViewButtonDelegate, ViewSoundDelegate {
    
    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails) {
        if keyWord == "Mute" {
            goToMute(key: "Mute", bookDetailss: bookDetils)
        } else if keyWord == "Unmute" {
            goToMute(key: "Unmute", bookDetailss: bookDetils)
        } else if keyWord == "Play" {
            goToPlay(key: "Play", bookDetailss: bookDetils)
        } else if keyWord == "Pause" {
            goToPlay(key: "Pause", bookDetailss: bookDetils)
        }
    }
    
    
    func didButtonPressed(keyWord: String) {
        if keyWord == "Author" {
            self.goToAuthors()
        } else if keyWord == "Favourite" {
            self.goToFavourites()
        } else if keyWord == "Home" {
//            self.checkedForLoggedInUser()
        }
    }
    
}

//extension HomeViewController : UICollectionViewDelegateFlowLayout{
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = (collectionView.bounds.width - 40 )/3.0
//        return CGSize (width: width, height: collectionView.bounds.height-20)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets.init(top: 10 , left: 10, bottom: 10, right: 10)
//    }
//
//
//}

