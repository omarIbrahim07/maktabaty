//
//  HomeCollectionViewCell.swift
//  Maktbty
//
//  Created by Gado on 6/27/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import Cosmos

class HomeCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bookImage.addCornerRadius(raduis: 13, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
    }
    
    func setData(text: String)  {
        self.bookName.setTitle(text, for: .normal)
    }
    
    @IBOutlet weak var bookName: UIButton!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var favoritesCountButton: UIButton!
    @IBOutlet weak var commentsCountButton: UIButton!
    @IBOutlet weak var averageRateCosmosView: CosmosView!
    

}
