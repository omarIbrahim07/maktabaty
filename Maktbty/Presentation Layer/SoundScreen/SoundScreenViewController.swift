//
//  SoundScreenViewController.swift
//  Maktbty
//
//  Created by Gado on 6/17/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import SwiftAudio
import AVFoundation
import MediaPlayer

var window: UIWindow?

protocol ButtonPartTableViewCellDelegate {
    func didButtonPartPressed(tag: Int)
}

class SoundScreenViewController: BaseViewController {
    
    var soundScreenViewControllerBookDetails : BookDetails?
    var bookImage : UIImage?
    private var isScrubbing: Bool = false
    let player = AudioPlayer()
    private let controller = AudioController.shared
    private var lastLoadFailed: Bool = false
    var isPaid: Bool? = false
    
    var mute: String?
    var play: String?
    var playAgain: String?
    var index: Int?
    var dest: String?
        
//    var soundDelegate: ViewSoundDelegate?
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var soundImage: UIImageView!
    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var favoritesCountButton: UIButton!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var elapsedTimeLabel: UILabel!
    @IBOutlet weak var soundSlider: UISlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var bookPartsButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    //MARK:- Delegate Helpers
//    func didSoundDisapeared(bookDetils: BookDetails) {
//        if let delegateValue = soundDelegate {
//            delegateValue.didSoundDisapeared(bookDetils: bookDetils)
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if playAgain != "P" {
            setupPlayer()
        }
        
        configureView()
        configureAudioPlayerEventHandlers()
        
        if mute == "Mute" {
            try? controller.player.isMuted = true
            UserDefaults.standard.set("Muted", forKey: "mute") //Bool
//            let u =  UserDefaults.standard.string(forKey: "mute")
//            print("U: \(u)")
            navigationController?.popViewController(animated: true)

            dismiss(animated: true, completion: nil)

        } else if mute == "Unmute" {
            try? controller.player.isMuted = false
            UserDefaults.standard.set("Unmuted", forKey: "mute") //Bool
//            let u =  UserDefaults.standard.string(forKey: "mute")
//            print("U: \(u)")
            navigationController?.popViewController(animated: true)
            
            dismiss(animated: true, completion: nil)

        }
        
        if play == "Play" {
            try? controller.player.play()
            UserDefaults.standard.set("Played", forKey: "play") //Bool
//            let u =  UserDefaults.standard.string(forKey: "play")
//            print("U: \(u)")
            navigationController?.popViewController(animated: true)
            
            dismiss(animated: true, completion: nil)

        } else if play == "Pause" {
            try? controller.player.pause()
            UserDefaults.standard.set("Paused", forKey: "play") //Bool
//            let u =  UserDefaults.standard.string(forKey: "play")
//            print("U: \(u)")

            navigationController?.popViewController(animated: true)
            
            dismiss(animated: true, completion: nil)

        }
        
    }
    
    func configureView() {
        soundImage.layer.borderWidth = 1
        soundImage.layer.masksToBounds = false
        soundImage.layer.borderColor = UIColor.black.cgColor
        soundImage.layer.cornerRadius = soundImage.frame.height/2
        soundImage.clipsToBounds = true
        soundImage.loadImageFromUrl(imageUrl: soundScreenViewControllerBookDetails!.image)
        bookNameLabel.text = soundScreenViewControllerBookDetails?.title
        authorNameLabel.text = soundScreenViewControllerBookDetails?.author
        favoritesCountButton.setTitle(String(soundScreenViewControllerBookDetails!.heartsCount), for: .normal)
//        soundSlider.setThumbImage(UIImage(named: "circle"), for: UIControl.State.normal)
        soundSlider.setThumbImage(#imageLiteral(resourceName: "circle"), for: UIControl.State.normal)
    }
    
    func setupPlayer() {
        
//        if controller.sources.count > 0 {
//            controller.player.reset()
//             try? controller.player.load(item: DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[0])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage), playWhenReady: true)
//            controller.add()
//
//            for i in 1..<soundScreenViewControllerBookDetails!.audio.count {
////                controller.player.reset()
//                controller.sources.append(DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[i])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage))
//            }
//
//            controller.add()
//
//            return
//        }
//
//        for i in 0..<soundScreenViewControllerBookDetails!.audio.count {
////            controller.player.reset()
//            controller.sources.append(DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[i])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage))
//        }
        
        
        
  /////////////////////////////////////////////////////////////////////// OLD
        
//MARK:- The best solution for sound
//        if controller.sources.count > 0 {
//
//            // To reset the current sound
//            controller.player.reset()
//
//            print(controller.sources.count)
//
//            // load to remove any previous first book track
//             try? controller.player.load(item: DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[0])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage), playWhenReady: true)
//
//            controller.player.reset()
//            controller.sources = []
//
//        }
//
//        // append all book tracks
//        for i in 0..<soundScreenViewControllerBookDetails!.audio.count {
//            controller.sources.append(DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[i])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage))
//            self.index = Int((soundScreenViewControllerBookDetails?.audio[i])!)
//        }
//        // To add the sound tracks to the array, so if u remove this the app will crash as the sounds in nil
//        controller.add()
//
//        print(controller.sources)
        // MARK:- The new solution
        if controller.sources.count > 0 {

            // To reset the current sound
            controller.player.reset()
            
            print(controller.sources.count)
            
            // load to remove any previous first book track
             try? controller.player.load(item: DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[0])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage), playWhenReady: true)

            controller.player.reset()
            controller.sources = []
                        
        }
        
        // append first track sound of audiobook only
//        if self.isPaid == false {
        if self.soundScreenViewControllerBookDetails?.isPayment == false {
            for i in 0..<1 {
                controller.sources.append(DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[i])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage))
                self.index = Int((soundScreenViewControllerBookDetails?.audio[i])!)
            }
            // To add the sound tracks to the array, so if u remove this the app will crash as the sounds in nil
            controller.add()

            print(controller.sources)
        }

        // append all book tracks
//        if self.isPaid == true {
        if self.soundScreenViewControllerBookDetails?.isPayment == true {
            for i in 0..<soundScreenViewControllerBookDetails!.audio.count {
                controller.sources.append(DefaultAudioItem(audioUrl: (soundScreenViewControllerBookDetails?.audio[i])!, artist: soundScreenViewControllerBookDetails?.author, title: soundScreenViewControllerBookDetails?.title, albumTitle: soundScreenViewControllerBookDetails?.title, sourceType: .stream, artwork: bookImage))
                self.index = Int((soundScreenViewControllerBookDetails?.audio[i])!)
            }
            // To add the sound tracks to the array, so if u remove this the app will crash as the sounds in nil
            controller.add()

            print(controller.sources)
        }

    }
    

    func jumpToPart(part: Int) {
        try? controller.player.jumpToItem(atIndex: part, playWhenReady: true)
    }


    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationHomeViewController")
//
//            UIApplication.shared.keyWindow?.rootViewController = viewController
//
//        print("Removed")
        
        
        if let evaluateNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationHomeViewController") as? UINavigationController, let rootViewContoller = evaluateNavigationController.viewControllers[0] as? HomeViewController {
            rootViewContoller.bookDetails = soundScreenViewControllerBookDetails
            
            UserDefaults.standard.set(soundScreenViewControllerBookDetails?.id, forKey: "ID") //Bool
            
            if controller.player.isMuted == false {
                print("Unmuteeeeeeeeeeeeeeeeeeed")
                UserDefaults.standard.set("Unmuted", forKey: "mute") //Bool
                rootViewContoller.mute = "Unmuted"
            } else if controller.player.isMuted == true {
                print("muteeeeeeeeeeeeeeeeeeed")
                UserDefaults.standard.set("Muted", forKey: "mute") //Bool
                rootViewContoller.mute = "Muted"
            }
            
            if controller.player.playerState == .playing || controller.player.playerState == .buffering || controller.player.playerState == .loading {
                print("Plaaaaaayed")
                UserDefaults.standard.set("Played", forKey: "play") //Bool
                rootViewContoller.play = "Played"
            } else if controller.player.playerState == .paused {
                print("Pausssssssed")
                UserDefaults.standard.set("Paused", forKey: "play") //Bool
                rootViewContoller.play = "Paused"
            }
            UIApplication.shared.keyWindow?.rootViewController = evaluateNavigationController

            print("Fes")
        }
    }

    @IBAction func bookPartsButtonTapped(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "BookPartsViewController") as?  BookPartsViewController
        destinationVC!.bookPartsViewControllerBookDetails = soundScreenViewControllerBookDetails
        destinationVC?.PartButtonDelegate = self
        self.present(destinationVC!, animated: true)
//        let presentedVC = BookPartsViewController()
//        presentedVC.PartButtonDelegate = self
//        present(presentedVC, animated: true, completion: nil)
    }
    
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
        let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
        return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x + 8, y: slider.frame.origin.y + 20)
    }

    //-------------------
    
    @IBAction func togglePlay(_ sender: Any) {
        if !controller.audioSessionController.audioSessionIsActive {
            try? controller.audioSessionController.activateSession()
        }
        if lastLoadFailed, let item = controller.player.currentItem {
            lastLoadFailed = false
            errorLabel.isHidden = true
            try? controller.player.load(item: item, playWhenReady: true)
        }
        else {
            controller.player.togglePlaying()
        }
    }
    
    @IBAction func startScrubbing(_ sender: UISlider) {
        isScrubbing = true
    }
    
    @IBAction func scrubbing(_ sender: UISlider) {
        controller.player.seek(to: Double(soundSlider.value))
    }
    
    @IBAction func scrubbingValueChanged(_ sender: UISlider) {
        let value = Double(soundSlider.value)
        elapsedTimeLabel.text = value.secondsToString()
        remainingTimeLabel.text = (controller.player.duration - value).secondsToString()
    }
    
    func updateTimeValues() {
        self.soundSlider.maximumValue = Float(self.controller.player.duration)
        self.soundSlider.setValue(Float(self.controller.player.currentTime), animated: true)
        self.elapsedTimeLabel.text = self.controller.player.currentTime.secondsToString()
        self.remainingTimeLabel.text = (self.controller.player.duration - self.controller.player.currentTime).secondsToString()
    }
    
    func updateMetaData() {
        if let item = controller.player.currentItem {
            bookNameLabel.text = item.getTitle()
            authorNameLabel.text = item.getArtist()
            item.getArtwork({ (image) in
                self.soundImage.image = image
            })
        }
    }
    
    // MARK: - AudioPlayer Event Handlers
    
    
    
    func configureAudioPlayerEventHandlers() {
        controller.player.event.stateChange.addListener(self, handleAudioPlayerStateChange)
        controller.player.event.secondElapse.addListener(self, handleAudioPlayerSecondElapsed)
        controller.player.event.seek.addListener(self, handleAudioPlayerDidSeek)
        controller.player.event.updateDuration.addListener(self, handleAudioPlayerUpdateDuration)
        controller.player.event.didRecreateAVPlayer.addListener(self, handleAVPlayerRecreated)
        controller.player.event.fail.addListener(self, handlePlayerFailure)
        updateMetaData()
        handleAudioPlayerStateChange(data: controller.player.playerState)
    }
    
    func setPlayButtonState(forAudioPlayerState state: AudioPlayerState) {
        playButton.setImage(state == .playing ? #imageLiteral(resourceName: "pause (1)") : #imageLiteral(resourceName: "play-button-1"), for: .normal)
    }
    
    
    func setErrorMessage(_ message: String) {
        self.loadIndicator.stopAnimating()
        errorLabel.isHidden = false
        errorLabel.text = message
    }
    
    func handleAudioPlayerStateChange(data: AudioPlayer.StateChangeEventData) {
        print(data)
        DispatchQueue.main.async {
            self.setPlayButtonState(forAudioPlayerState: data)
            switch data {
            case .loading:
//                self.startLoading()
                self.loadIndicator.startAnimating()
                self.updateMetaData()
//                self.updateTimeValues()
            case .buffering:
//                self.startLoading()
                self.updateTimeValues()

                self.loadIndicator.startAnimating()
            case .ready:
//                self.startLoading()

                self.loadIndicator.stopAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .playing, .paused, .idle:
//                self.startLoading()

                self.loadIndicator.stopAnimating()
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerSecondElapsed(data: AudioPlayer.SecondElapseEventData) {
        if !isScrubbing {
            DispatchQueue.main.async {
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerDidSeek(data: AudioPlayer.SeekEventData) {
        isScrubbing = false
    }
    
    func handleAudioPlayerUpdateDuration(data: AudioPlayer.UpdateDurationEventData) {
        DispatchQueue.main.async {
            self.updateTimeValues()
        }
    }
    
    func handleAVPlayerRecreated() {
        try? controller.audioSessionController.set(category: .playback)
    }
    
    func handlePlayerFailure(data: AudioPlayer.FailEventData) {
        if let error = data as NSError? {
            if error.code == -1009 {
                lastLoadFailed = true
                DispatchQueue.main.async {
//                    self.setErrorMessage("Network disconnected. Please try again...")
                }
            }
        }
    }
    
    func shareBook(shareLink: String) {
        
        if let name = NSURL(string: shareLink) {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
        else
        {
            // show alert for not available
        }
    }
    
    func favouriteBook() {
        
        let memId = UserDefaultManager.shared.currentUser?.userID
        
        guard let bookId = self.soundScreenViewControllerBookDetails?.id, bookId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الكتاب"
            showError(error: apiError)
            return
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : memId as AnyObject,
            "like_id" : bookId as AnyObject,
            "type" : "like" as AnyObject
        ]
        
        self.startLoading()
        
        CategoriesAPIManager().favouriteBook(basicDictionary: parameters , onSuccess: { (message) in
            
            self.stopLoadingWithSuccess()
            let apiError = APIError()
            apiError.message = message
            self.showError(error: apiError)
            
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    @IBAction func shareButtonIsPressed(_ sender: Any) {
        print("Share Is Pressed")
        
        if let link = soundScreenViewControllerBookDetails?.shareLink {
            shareBook(shareLink: link)
        }
        
    }
    
    @IBAction func addToFavouritesIsPressed(_ sender: Any) {
        print("Favourite Is Pressed")
        favouriteBook()
    }
    
    
    @IBAction func previousPartIsPressed(_ sender: Any) {
        print("Previous is pressed")
            try? controller.player.previous()
    }
    
    @IBAction func nextPartIsPressed(_ sender: Any) {
        print("next is pressed")
//        if self.isPaid == true {
        if self.soundScreenViewControllerBookDetails?.isPayment == true {
            try? controller.player.next()
        }
    }
    
}

//MARK: Solution of Thumb image does not move to the edge of UISlider
class SoundSlider : UISlider {
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect
    {
        let unadjustedThumbrect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        let thumbOffsetToApplyOnEachSide:CGFloat = unadjustedThumbrect.size.width / 2.0
        let minOffsetToAdd = -thumbOffsetToApplyOnEachSide
        let maxOffsetToAdd = thumbOffsetToApplyOnEachSide
        let offsetForValue = minOffsetToAdd + (maxOffsetToAdd - minOffsetToAdd) * CGFloat(value / (self.maximumValue - self.minimumValue))
        var origin = unadjustedThumbrect.origin
        origin.x += offsetForValue
        return CGRect(origin: origin, size: unadjustedThumbrect.size)
    }
}

extension SoundScreenViewController: ButtonPartTableViewCellDelegate {
    
    func didButtonPartPressed(tag: Int) {
        print("Try")
        jumpToPart(part: tag)
    }

//    func didButtonPartPressed(tag: Int) {
//        if let delegateValue = PartButtonDelegate {
//            delegateValue.didButtonPartPressed(tag: tag)
//        }
//    }
}

extension SoundScreenViewController: ChildViewControllerDelegate {
    // Define Delegate Method
    func childViewControllerResponse(mute: String, play: String, index: Int) {
        print("7obby")
        self.mute = mute
        self.play = play
        self.index = index
    }

}

//extension SoundScreenViewController: ViewSoundDelegate {
//
//    func didSoundDisapeared(bookDetils: BookDetails) {
//        self.soundDelegate?.didSoundDisapeared(bookDetils: self.soundScreenViewControllerBookDetails!)
//    }
//
//
////    func didBookButtonPartPressed(tag: Int) {
////        print("7obby")
////        self.dismiss(animated: true, completion: nil)
////        self.PartButtonDelegate?.didButtonPartPressed(tag: tag)
////    }
//}
