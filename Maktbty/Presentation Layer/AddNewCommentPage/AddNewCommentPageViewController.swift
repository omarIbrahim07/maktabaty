//
//  CommentPageViewController.swift
//  Maktbty
//
//  Created by Gado on 5/26/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import Cosmos

class AddNewCommentPageViewController: BaseViewController {
    
    var authorRate: Double?
    var bookRate: Double?
    var bookID: Int?
    var authorID: Int?
    var type: String?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var ratingCosmosView: CosmosView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        rate()
    }
    
    func configureView() {
        sendButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        shareButton.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
    }
    
    func rate() {
        
        if type == "Book" {
            ratingCosmosView.didTouchCosmos = { rating in
                self.bookRate = self.ratingCosmosView.rating
                print(self.bookRate)
                self.rateSomething()
            }
        } else if type == "Author" {
            ratingCosmosView.didTouchCosmos = { rating in
                self.authorRate = self.ratingCosmosView.rating
                print(self.authorRate)
                self.rateSomething()
            }
        }
        
    }
    
    func postComment() {
        
        if type == "Book" {
        
        guard let name = self.nameTextField.text, name.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال إسمك"
            showError(error: apiError)
            return
        }
        
        guard let email = self.emailTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال البريد الإلكتروني"
            showError(error: apiError)
            return
        }
        
        guard let comment = self.commentTextField.text, comment.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال تعليقك"
            showError(error: apiError)
            return
        }
        
        guard let bookId = self.bookID, bookId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الكتاب"
            showError(error: apiError)
            return
        }
        
        let parameters: [String : AnyObject] = [
            "user_id" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
            "comment" : comment as AnyObject,
            "name" : name as AnyObject,
            "email" : email as AnyObject,
            "book_id" : bookID as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        CategoriesAPIManager().postCommentOnAuthor(basicDictionary: parameters, onSuccess: { (message) in
            
        self.stopLoadingWithSuccess()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        } else if type == "Author" {
            
            guard let name = self.nameTextField.text, name.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال إسمك"
                showError(error: apiError)
                return
            }
            
            guard let email = self.emailTextField.text, email.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال البريد الإلكتروني"
                showError(error: apiError)
                return
            }
            
            guard let comment = self.commentTextField.text, comment.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال تعليقك"
                showError(error: apiError)
                return
            }
            
            guard let authorId = self.authorID, authorId > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال الكاتب"
                showError(error: apiError)
                return
            }
            
            let parameters: [String : AnyObject] = [
                "user_id" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
                "comment" : comment as AnyObject,
                "name" : name as AnyObject,
                "email" : email as AnyObject,
                "author_id" : authorId as AnyObject
            ]
            
            weak var weakSelf = self
            
            startLoading()
            
            CategoriesAPIManager().postCommentOnAuthor(basicDictionary: parameters, onSuccess: { (message) in
                
                self.stopLoadingWithSuccess()
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
            
        }
        
    }
    
    func rateSomething() {
        
        if type == "Author" {
            
            guard let authorId = self.authorID, authorId > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال الكاتب"
                showError(error: apiError)
                return
            }
            
            guard let rating = self.authorRate, rating > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال التقييم"
                showError(error: apiError)
                return
            }
            
            let parameters: [String : AnyObject] = [
                "user_id" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
                "author_id" : authorId as AnyObject,
                "rating" : rating as AnyObject
            ]
            
            weak var weakSelf = self
            
            startLoading()
            
            CategoriesAPIManager().rateAuthor(basicDictionary: parameters, onSuccess: { (message) in
                
            self.stopLoadingWithSuccess()
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
            
            
        } else if type == "Book" {
            
            guard let bookId = self.bookID, bookId > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال الكتاب"
                showError(error: apiError)
                return
            }
            
            guard let rating = self.bookRate, rating > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال التقييم"
                showError(error: apiError)
                return
            }
            
            let parameters: [String : AnyObject] = [
                "author_id" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
                "book_id" : bookId as AnyObject,
                "rating" : rating as AnyObject
            ]
            
            weak var weakSelf = self
            
            startLoading()
            
            CategoriesAPIManager().rateBook(basicDictionary: parameters, onSuccess: { (message) in
                
                self.stopLoadingWithSuccess()
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
            
        }
            
            
        
    }
    
    
    @IBAction func sendCommentIsPressed(_ sender: Any) {
        postComment()
    }
    
}
