//
//  CommentsPageViewController.swift
//  Maktbty
//
//  Created by Gado on 6/25/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class CommentsPageViewController: BaseViewController {
    
    var myComments: [CommentsPreviews] = []
    var authorId: Int?
    var bookId: Int?
    var flag = 0
    


//    var commentArray:[(userName: String, comment: String)] = [("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("xName","comment app is so so so so so so so so so so so so disgusting"),("جورج","كلامك ياحبيبي .. كلام غير الكلامبيطمن قلبى ديما .. ينسينى الالامكل اما تشوفنى قلى .fffffffffffffffffff ggggggggggggggggffffffff ggggffffffff fffffffffffff. بتاثر فيا كلىوانا بعدك ايه فضلى .. ايه على الدنيا")]
    


    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var newCommentButton: UIButton!
    @IBAction func newCommentButtonPressed(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: "Main", bundle:     nil)
        let destinationVC = sb.instantiateViewController(
            withIdentifier: "AddNewCommentPageViewController") as?  AddNewCommentPageViewController
        destinationVC?.bookID = bookId
        destinationVC?.authorID = authorId
        
        if flag == 1 {
            destinationVC?.type = "Author"
        } else if flag == 2 {
            destinationVC?.type = "Book"
        }
        
        self.navigationController?.pushViewController(destinationVC!, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureTableView()
        configureView()
        
        switch flag {
        case 1:
            getAuthorCommentsPreviews()
        case 2:
            getBookCommentsPreviews()
        default:
            return
            
        }
      
        
    }
    
    
    func configureView() {
    }
    
    func configureTableView() {
        commentsTableView.dataSource = self
        commentsTableView.delegate = self
        commentsTableView.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
    }

    func getAuthorCommentsPreviews() {
        
        guard let authorID = self.authorId, authorID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الكاتب"
            showError(error: apiError)
            return
        }
        
        startLoading()
        
        CategoriesAPIManager().getAuthorCommentsPreviews(authorId: authorID ,basicDictionary: [:], onSuccess: { (comments) in
            
            self.stopLoadingWithSuccess()
            
            self.myComments = comments
            self.commentsTableView.reloadData()
        }) { (error) in
            print(error)
        }
        
    }
    
    
    func getBookCommentsPreviews() {
        
        guard let bookID = self.bookId, bookID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الكتاب"
            showError(error: apiError)
            return
        }
        
        startLoading()
        
        CategoriesAPIManager().getBookCommentsPreviews(bookId: bookID ,basicDictionary: [:], onSuccess: { (comments) in
            
            self.stopLoadingWithSuccess()
            
            self.myComments = comments
            self.commentsTableView.reloadData()
        }) { (error) in
            print(error)
        }
        
    }
    
    
}

extension CommentsPageViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
        cell.userNameLabel.text = myComments[indexPath.row].userName
        cell.commentLabel.text = myComments[indexPath.row].message
        return cell
    }
    
}
