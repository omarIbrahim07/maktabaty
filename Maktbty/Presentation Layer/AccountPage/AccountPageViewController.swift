//
//  AccountPageViewController.swift
//  Maktbty
//
//  Created by Gado on 5/22/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import DLRadioButton
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class AccountPageViewController: BaseViewController {
    
    
    @IBOutlet weak var accountImageView: UIImageView!
    @IBOutlet weak var subscriptionStateButton: DLRadioButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var subscriptionstateTextFieldReadOnly: UITextField!
    @IBOutlet weak var bundleCategoryTextField: UITextField!
    @IBOutlet weak var expiryDateTextField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        appendDataProfile()
//        getAccountDetails()

        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        nameTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        emailTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        subscriptionstateTextFieldReadOnly.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        bundleCategoryTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        expiryDateTextField.addCornerRadius(raduis: 19, borderColor: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), borderWidth: 0.5)
        accountImageView.layer.borderWidth = 1
        accountImageView.layer.masksToBounds = false
        accountImageView.layer.borderColor = UIColor.black.cgColor
        accountImageView.layer.cornerRadius = accountImageView.frame.height/2
        accountImageView.clipsToBounds = true
        self.title = "الملف الشخصي"
    }
    
    func appendDataProfile() {
        
        if let userImage = UserDefaultManager.shared.currentUser?.photo {
            accountImageView.loadImageFromUrl(imageUrl: userImage)
        }
        
        if let userName = UserDefaultManager.shared.currentUser?.name {
            nameTextField.text = userName
        }
        
        if let userEmail = UserDefaultManager.shared.currentUser?.email {
            emailTextField.text = userEmail
        }
    }
    
//    func getAccountDetails() {
    
//        startLoading()
//
//        SideMenuAPIManager().getAccountDetails(userId: 20, basicDictionary: [:], onSuccess: { (accountDetails) in
//
//            self.stopLoadingWithSuccess()
//            self.myAccountDetails = accountDetails
//            print("test\(self.myAccountDetails!)")
//            self.accountImageView.loadImageFromUrl(imageUrl: self.myAccountDetails!.images[0])
//            self.emailTextField.text = self.myAccountDetails!.email
//            self.nameTextField.text = self.myAccountDetails!.userName
//        }) { (error) in
//            print(error)
//        }
        
//    }

    @IBAction func subscriptionButton(_ sender: DLRadioButton) {
        
//        if sender.tag == 0
//        {
//            print("subsriped")
//        }
//        else
//        {
//            print("unsubscriped")
//        }
    }

}


