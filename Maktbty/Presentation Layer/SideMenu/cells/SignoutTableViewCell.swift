//
//  SignoutTableViewCell.swift
//  Maktbty
//
//  Created by Gado on 7/25/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

protocol logOutTableViewCellDelegate {
    func didLogOutButtonPressed()
}

class SignoutTableViewCell: UITableViewCell {
    
    var logoutButtonDelegate: logOutTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        signOutButton.imageView?.contentMode = .scaleAspectFit

    }
    
    @IBOutlet weak var signOutButton: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Delegate Helpers
    func didLogOutButtonPressed() {
        if let delegateValue = logoutButtonDelegate {
            delegateValue.didLogOutButtonPressed()
        }
    }
    
    @IBAction func logOutIsPressed(_ sender: Any) {
        didLogOutButtonPressed()
    }
    
    
}
