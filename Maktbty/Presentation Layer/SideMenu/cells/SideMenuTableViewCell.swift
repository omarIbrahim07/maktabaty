//
//  SideMenuTableViewCell.swift
//  Maktbty
//
//  Created by Gado on 6/20/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
         sideMenuItemLabel.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), borderWidth: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var sideMenuItemLabel: UILabel!
    
}
