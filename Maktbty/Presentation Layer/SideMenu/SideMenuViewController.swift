//
//  SideMenuViewController.swift
//  Maktbty
//
//  Created by Gado on 6/20/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit


class SideMenuViewController: UIViewController {

    @IBOutlet weak var sideMenuTableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureSideMenu()
        configureView()
        getUserProfile()
    }
    var sideMenuArray = [
        "الرئيسية"
        ,"سلة التسوق"
        ,"مكتبتي"
        ,"الصفحة الشخصية"
        ,"المفضلة"
        ,"الاشتراكات"
        ,"كود التسجيل"
        ,"كود التخفيض"
        ,"إعداد الحساب"
        ,"كتب الاشتراك"
        ,"كتب للبيع"
        ,"كتب مجانية"
        ,"حول التطبيق"
    ]
    
    
    func configureView() {
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        
        profileImage.loadImageFromUrl(imageUrl: UserDefaultManager.shared.currentUser!.photo!)
//        print(UserDefaultManager.shared.currentUser?.name as Any)
        
        userNameLabel.text = UserDefaultManager.shared.currentUser?.name
        userEmailLabel.text = UserDefaultManager.shared.currentUser?.email
    }
    
    func configureSideMenu()  {
        
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        sideMenuTableView.register(UINib(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableViewCell")
         sideMenuTableView.register(UINib(nibName: "SignoutTableViewCell", bundle: nil), forCellReuseIdentifier: "SignoutTableViewCell")
        
    }
    
    func getUserProfile() {
        
        let parameters  = [
            "ID" : UserDefaultManager.shared.currentUser?.userID as AnyObject,
        ]
        
        AuthenticationAPIManager().getUserProfile(basicDictionary: parameters, onSuccess: { (user) in
            
        }) { (error) in
        }
        
    }
    
    func logOut() {
        
        UserDefaultManager.shared.currentUser = nil
        if let navigationControllerLogin = storyboard?.instantiateViewController(withIdentifier: "NavigationControllerLogin") as? UINavigationController, let rootViewContoller = navigationControllerLogin.viewControllers[0] as? LoginAndSignupPageViewController {
            UIApplication.shared.keyWindow?.rootViewController = navigationControllerLogin
        }
    }

}

extension SideMenuViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  sideMenuArray.count != 0 ? sideMenuArray.count + 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == sideMenuArray.count
        {
            let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: "SignoutTableViewCell") as! SignoutTableViewCell
            
            cell.logoutButtonDelegate = self
            
            return cell


        } else
        {
        let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell

        cell.sideMenuItemLabel.text = sideMenuArray[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.blue
        return cell
        }
    }
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            dismiss(animated: true, completion: nil)
            return
//            let sb = UIStoryboard.init(name: "Main", bundle:     nil)
//            let destinationVC = sb.instantiateViewController(
//                withIdentifier: "HomeViewController") as?  HomeViewController
//            self.navigationController?.pres(destinationVC!, animated: true)
        case 1:
            if let basketBooksNavigationViewController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "ShoppingBasketNavigationViewController") as? UINavigationController, let _ = basketBooksNavigationViewController.viewControllers[0] as? ShoppingBasketViewController {
                self.present(basketBooksNavigationViewController, animated: true, completion: nil)
            }
        case 2:
            if let myGenericBooksScreensNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenericBooksScreensNavigationController") as? UINavigationController, let rootViewController = myGenericBooksScreensNavigationController.viewControllers[0] as? GenericBooksScreensViewController {
                rootViewController.flag = 4
                self.present(myGenericBooksScreensNavigationController, animated: true, completion: nil)
            }
        case 3:
            if let myAccountPageNavigationViewController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "AccountPageNavigationViewController") as? UINavigationController, let _ = myAccountPageNavigationViewController.viewControllers[0] as? AccountPageViewController {
                self.present(myAccountPageNavigationViewController, animated: true, completion: nil)
            }
        case 4:
            if let myFavouriteNavigationViewController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "FavouriteNavigationViewController") as? UINavigationController, let _ = myFavouriteNavigationViewController.viewControllers[0] as? FavouriteViewController {
                self.present(myFavouriteNavigationViewController, animated: true, completion: nil)
            }
        case 5:
            if let mySubscriptionsNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionsNavigationController") as? UINavigationController, let _ = mySubscriptionsNavigationController.viewControllers[0] as? SubscriptionsViewController {
                self.present(mySubscriptionsNavigationController, animated: true, completion: nil)
            }
        case 6:
            if let mySubscriptionCodeNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionCodeNavigationController") as? UINavigationController, let _ = mySubscriptionCodeNavigationController.viewControllers[0] as? SubscriptionCodeViewController {
                self.present(mySubscriptionCodeNavigationController, animated: true, completion: nil)
            }
        case 7:
            if let myDiscountCodeNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "DiscountCodeNavigationController") as? UINavigationController, let _ = myDiscountCodeNavigationController.viewControllers[0] as? DiscountCodeViewController {
                self.present(myDiscountCodeNavigationController, animated: true, completion: nil)
            }
        case 8:
            if let myAccountSettingsNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "AccountSettingsNavigationController") as? UINavigationController, let _ = myAccountSettingsNavigationController.viewControllers[0] as? AccountSettingsViewController {
                self.present(myAccountSettingsNavigationController, animated: true, completion: nil)
            }
        case 9:
            if let myGenericBooksScreensNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenericBooksScreensNavigationController") as? UINavigationController, let rootViewController = myGenericBooksScreensNavigationController.viewControllers[0] as? GenericBooksScreensViewController {
                rootViewController.flag = 3
                self.present(myGenericBooksScreensNavigationController, animated: true, completion: nil)
            }
        case 10:
            if let myGenericBooksScreensNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenericBooksScreensNavigationController") as? UINavigationController, let rootViewController = myGenericBooksScreensNavigationController.viewControllers[0] as? GenericBooksScreensViewController {
                rootViewController.flag = 2
                self.present(myGenericBooksScreensNavigationController, animated: true, completion: nil)
            }
        case 11:
            if let myGenericBooksScreensNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenericBooksScreensNavigationController") as? UINavigationController, let rootViewController = myGenericBooksScreensNavigationController.viewControllers[0] as? GenericBooksScreensViewController {
                rootViewController.flag = 1
                self.present(myGenericBooksScreensNavigationController, animated: true, completion: nil)
            }
        case 12:
            if let myAboutAppNavigationController = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "myAboutAppNavigationController") as? UINavigationController, let _ = myAboutAppNavigationController.viewControllers[0] as? AboutAppViewController {
                self.present(myAboutAppNavigationController, animated: true, completion: nil)
            }
        default:
            return
        }
     
    }
}

extension SideMenuViewController: logOutTableViewCellDelegate {
    
    func didLogOutButtonPressed() {
        self.logOut()
    }
    
}
