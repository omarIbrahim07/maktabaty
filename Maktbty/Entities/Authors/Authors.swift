//
//  Authors.swift
//  Maktbty
//
//  Created by Gado on 7/25/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import Foundation
import ObjectMapper



class Authors : Mappable {
    
    var id : Int!
    var shareLink : String!
    var name : String!
    var notes : String!
    var averageRate : Int!
    var commentsCount : Int!
    var image : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        shareLink <- map["share_link"]
        name <- map["name"]
        notes <- map["notes"]
        averageRate <- map["voting"]
        commentsCount <- map["comments"]
        image <- map["img"]
        
    }
    
}


class AuthorDetails : Mappable {
    
    var id : Int!
    var shareLink : String!
    var commentsCount : Int!
    var name : String!
    var notes : String!
    var image : String!
    var averageRate : Int!
    var revisionsCount : Int!
    var readersCount : Int!
    var evaluation : Int!
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        shareLink <- map["share_link"]
        commentsCount <- map["comments"]
        name <- map["name"]
        notes <- map["notes"]
        image <- map["img"]
        averageRate <- map["voting"]
        revisionsCount <- map["revision"]
        readersCount <- map["readers"]
        evaluation <- map["evaluation"]
        
        
    }
    
}
