//
//  Account.swift
//  Maktbty
//
//  Created by Gado on 7/25/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import Foundation
import ObjectMapper


class AccountDetails : Mappable {
    
    var status : Bool!
    var Id : Int!
    var numberOfSalesBook : Int!
    var userName : String!
    var userMobileNumber : String!
    var email : String!
    var images : [String]!
  
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        status <- map["status"]
        Id <- map["ID"]
        numberOfSalesBook <- map["number_sales_book"]
        userName <- map["name_user"]
        userMobileNumber <- map["user_mobile"]
        email <- map["email"]
        images <- map["images"]

        
    }
    
}
