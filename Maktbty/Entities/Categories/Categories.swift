//
//  Categories.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/12/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable {
    
    var id : Int!
    var name : String!

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        name <- map["name"]

    }
    
}



class CommentsPreviews  : Mappable {

    var id : Int!
    var message : String!
    var userName : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        message <- map["message"]
        userName <- map["name_user"]
    }
    
}

class HomeSlider  : Mappable {
    
    var title : String!
    var image : String!
    var bookId : Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title <- map["title"]
        image <- map["img"]
        bookId <- map["book_id"]
    }
    
}

class Subscribtion : Mappable {
    
    var status : Bool!
    var data : SubscribtionData!
    var packages : [SubscribtionPackages]!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        status <- map["status"]
        data <- map["data"]
        packages <- map["pakages"]
    }
    
}

class SubscribtionData : Mappable {
    
    var title : String!
    var details : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title <- map["Title"]
        details <- map["Details"]
    }
    
}

class SubscribtionPackages : Mappable {
    
    var id : String!
    var packagePrice : Double!
    var title : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        packagePrice <- map["pakage_price"]
        title <- map["title"]
        
    }
    
}

class BasketBook: Mappable {
    
    var cartId : Int!
    var id : Int!
    var name : String!
    var authorId : Int!
    var authorName : String!
    var image : String!
    var price : Int!
    var priceOffer : Int!
    var quantity : Int!
    var currencyName : String!
    var bookPrice : Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        cartId <- map["cart_id"]
        id <- map["ID"]
        name <- map["name"]
        authorId <- map["auther_id"]
        authorName <- map["auther_name"]
        image <- map["main_img"]
        price <- map["price"]
        priceOffer <- map["price_offer"]
        quantity <- map["quantity"]
        currencyName <- map["currency_name"]
        bookPrice <- map["book_price"]

    }    
}


//class Free_Subscribtion_Sale_Book : Mappable {
//
//
//    var id : Int!
//    var shareLink : String!
//    var nameType : String!
//    var title : String!
//    var isLike : Bool!
//    var commentsCount : Int!
//    var favoritesCount : Int!
//    var averageRate : Int!
//    var audio : String!
//    var image : String!
//
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["ID"]
//        shareLink <- map["share_link"]
//        nameType <- map["name_type"]
//        title <- map["title"]
//        isLike <- map["islike"]
//        commentsCount <- map["comments"]
//        favoritesCount <- map["heart"]
//        averageRate <- map["voting"]
//        audio <- map["audio"]
//        image <- map["img"]
//
//    }
//
//}


//class SubCategories: Mappable {
//
//    var id : Int!
//    var name : String!
//    var image : String!
////    var postsCount : Int!
//    var parent : String!
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["id"]
//        name <- map["name"]
//        image <- map["picture"]
////        postsCount <- map["count"]
//        parent <- map["parent_id"]
//    }
//}
//
//class Country: Mappable {
//
//    var code : String!
//    var name : String!
//    var image : String!
//
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        code <- map["code"]
//        name <- map["asciiname"]
//        image <- map["background_image"]
//    }
//}
//
//class City: Mappable {
//
//    var id : Int!
//    var name : String!
//
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["id"]
//        name <- map["asciiname"]
//    }
//}
//
//class PostType: Mappable {
//
//    var id : Int!
//    var name : String!
//
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["id"]
//        name <- map["name"]
//    }
//}
//
//
//class SecondaryCategories: Mappable {
//
//    var id : Int!
//    var countryID : String!
//    var userId : String!
//    var categoryId : String!
//    var title : String!
//    var description : String!
//    var tags : String!
//    var price : String!
//    var negotiable : String!
//    var contactName : String!
//    var email : String!
//    var phone : String!
//    var address : String!
//    var cityID : String!
//    var visits : String!
//    var advertisementTime : String!
//    //    var name : String!
//    //    var image : String!
//    //    var parent : String!
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["id"]
//        countryID <- map["country_code"]
//        userId <- map["user_id"]
//        categoryId <- map["category_id"]
//        title <- map["title"]
//        description <- map["description"]
//        tags <- map["tags"]
//        price <- map["price"]
//        negotiable <- map["negotiable"]
//        contactName <- map["contact_name"]
//        email <- map["email"]
//        phone <- map["phone"]
//        address <- map["address"]
//        cityID <- map["city_id"]
//        visits <- map["visits"]
//        advertisementTime <- map["archived_at.date"]
//
//        //        name <- map["name"]
//        //        image <- map["picture"]
//        //        parent <- map["parent_id"]
//    }
//}


