//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var userID : Int?
    var name : String?
    var photo : String?
    var phone : String?
    var email : String?
    var numberOfSalesBooks : Int?
    var play: String?
    var mute: String?
    var indexx: Int?
//    var photos : [String]?
    

    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userID <- map["ID"]
        name <- map["name_user"]
        photo <- map["img1"]
//        photos <- map["images"]
        phone <- map["user_mobile"]
        email <- map["email"]
        numberOfSalesBooks <- map["number_sales_book"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.userID = aDecoder.decodeObject(forKey: "ID") as? Int
        self.name = aDecoder.decodeObject(forKey: "name_user") as? String
        self.photo = aDecoder.decodeObject(forKey: "img1") as? String
//        self.photos = aDecoder.decodeObject(forKey: "images") as? [String]
        self.phone = aDecoder.decodeObject(forKey: "user_mobile") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.numberOfSalesBooks = aDecoder.decodeObject(forKey: "number_sales_book") as? Int
        self.play = aDecoder.decodeObject(forKey: "play") as? String
        self.mute = aDecoder.decodeObject(forKey: "mute") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: "ID")
        aCoder.encode(name, forKey: "name_user")
        aCoder.encode(photo, forKey: "img1")
//        aCoder.encode(photos, forKey: "images")
        aCoder.encode(phone, forKey: "user_mobile")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(numberOfSalesBooks, forKey: "number_sales_book")
        aCoder.encode(play, forKey: "play")
        aCoder.encode(mute, forKey: "mute")

    }
    
}

class RegisterUser : Mappable {
    
    var status : Bool!
    var id : Int!
    var message : String!
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        status <- map["status"]
        id <- map["id"]
        message <- map["message"]
        
        
}
}
