//
//  Books.swift
//  Maktbty
//
//  Created by Gado on 7/25/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import Foundation
import ObjectMapper

class BooksByCategory : Mappable {
    
    var id : Int!
    var name : String!
    var bookType : String!
    var type : String!
    var title : String!
    var isLike : Bool!
    var commentsCount : Int!
    var heartsCount : Int!
    var averageRate : Int!
    var audio : String!
    var image : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        name <- map["name"]
        bookType <- map["name_type"]
        type <- map["type"]
        title <- map["title"]
        isLike <- map["islike"]
        commentsCount <- map["comments"]
        heartsCount <- map["heart"]
        averageRate <- map["voting"]
        audio <- map["audio"]
        image <- map["img"]
        
    }
    
}

class BookDetails : Mappable {
    
    
    
    var id : Int!
    var shareLink : String!
    var type : String!
    var title : String!
    var isLike : Bool!
    var price : Int!
    var notes : String!
    var averageRate : Int!
    var commentsCount : Int!
    var heartsCount : Int!
    var mainCategory : String!
    var mainCategoryId : Int!
    var subCategory : String!
    var author : String!
    var autherId : Int!
    var audio : [String]!
    var image : String!
    var isPayment : Bool!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        shareLink <- map["share_link"]
        type <- map["type"]
        title <- map["title"]
        isLike <- map["islike"]
        price <- map["price"]
        notes <- map["notes"]
        averageRate <- map["voting"]
        commentsCount <- map["comments"]
        heartsCount <- map["heart"]
        mainCategory <- map["main_category"]
        mainCategoryId <- map["main_category_id"]
        subCategory <- map["sub_category"]
        author <- map["auther"]
        autherId <- map["auther_id"]
        audio <- map["audio"]
        image <- map["img"]
        isPayment <- map["ispayment"]
        
    }
    
}

class SearchedBook : Mappable {
    
    var id : Int!
    var bookType : String!
    var type : String!
    var title : String!
    var isLike : Bool!
    var commentsCount : Int!
    var heartsCount : Int!
    var averageRate : Int!
    var audio : String!
    var image : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["ID"]
        bookType <- map["name_type"]
        type <- map["type"]
        title <- map["title"]
        isLike <- map["islike"]
        commentsCount <- map["comments"]
        heartsCount <- map["heart"]
        averageRate <- map["voting"]
        audio <- map["audio"]
        image <- map["img"]
        
    }
    
}

