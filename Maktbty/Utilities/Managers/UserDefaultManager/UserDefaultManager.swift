//
//  UserDefaultManager.swift
//  GameOn
//
//  Created by Hassan on 2/25/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

class UserDefaultManager: NSObject {

    static var shared = UserDefaultManager()
    
    var authorization: String? {
        set {
            if let value = newValue {
                kUserDefault.set(value, forKey: kAuthorization)
                kUserDefault.synchronize()
            }
            else {
                kUserDefault.removeObject(forKey: kAuthorization)
                kUserDefault.synchronize()
            }
        }
        get {
            return kUserDefault.value(forKey: kAuthorization) as? String
        }
    }
    
    var currentUser : User? {
        set {
            if newValue != nil {
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: newValue!)
                kUserDefault.set(encodedData, forKey: kUserData)
                kUserDefault.synchronize()
            }
            else {
                kUserDefault.removeObject(forKey: kUserData)
                kUserDefault.synchronize()
            }
        }
        get {
            if let decodedNSDataBlob = kUserDefault.object(forKey: kUserData) as? NSData {
                if let savedUser = NSKeyedUnarchiver.unarchiveObject(with: decodedNSDataBlob as Data) as? User {
                    return savedUser
                }
            }
            return nil
        }
    }
}
