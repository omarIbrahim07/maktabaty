////
////  OmarFile.swift
////  Maktbty
////
////  Created by Gado on 7/31/19.
////  Copyright © 2019 Gado. All rights reserved.
////
//
//import Foundation
//
////
////  TelawaManager.swift
////  Quranana
////
////  Created by Omar Ali on 4/23/19.
////  Copyright © 2019 Omar Ali. All rights reserved.
////
//
//import Foundation
//import AVFoundation
//import AVKit
//
//
//
//
//class TelawaManager: NSObject, AVAudioPlayerDelegate {
//
//    deinit {
//        print("")
//    }
//
//    func changeAyah(ayahInfo: AyahInfo) {
//        currentAyah = ayahInfo
//        playFromStart()
//    }
//
//    var currentAyah: AyahInfo
//
//    private let player = AVQueuePlayer()
//    var percentageOfCurrentSurahChanged: ((Float) -> Void)?
//    init(ayahInfo: AyahInfo) {
//        self.currentAyah = ayahInfo
//        super.init()
//    }
//    var currentPage = 1
//    var currentRecitation: QuranRecitation?
//    func getSelectedRecitation(_ complete: @escaping (QuranRecitation) -> Void) {
//
//        if let _ = UserDefaultsManager.shared.recitations, let currentRecitation = UserDefaultsManager.shared.currentRecitation {
//            self.currentRecitation = currentRecitation
//            complete(currentRecitation)
//        }
//        else {
//            GetQuranRecitationsRequest().execute(onSuccess: {
//                [weak self]recitations in
//                UserDefaultsManager.shared.recitations = recitations
//                UserDefaultsManager.shared.currentRecitation = recitations.first
//                self?.currentRecitation = recitations.first
//                if let recitation = self?.currentRecitation {
//                    complete(recitation)
//                }
//                }, onFailure: {
//                    error in
//                    print("Hello")
//            })
//        }
//
//    }
//
//    func playFromStartOfCurrentAyah() {
//        playFromStart()
//    }
//
//    var currentRecitationUrl: String? {
//        guard let path = currentRecitation?.quranRecitationPath else {
//            return nil
//        }
//        return fullUrl(for: path)
//    }
//
//    func continuePlaying() {
//        player.play()
//    }
//
//    func playFromStart() {
//
//        guard let urlString = currentRecitationUrl else {
//            return
//        }
//
//
//
//        let dbManager = MushafDBManager.shared
//        currentPage = dbManager.getPage(ayahInfo: currentAyah)
//        let ayahInfosForPage = dbManager.getAyahs(page: currentPage)
//        var playerItems = ayahInfosForPage.map { (ayahInfo) -> AVPlayerItem in
//            let url = getUrlSurahUrl(url: urlString, ayahInfo: ayahInfo)
//            return AVPlayerItem(url: URL(string: url)!)
//        }
//
//        let index = ayahInfosForPage.firstIndex(where: {$0.uniqueAyahString == currentAyah.uniqueAyahString}) ?? 0
//        playerItems.removeFirst(index)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(didFinishAyahRecitation), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
//        player.removeAllItems()
//        playerItems.forEach({player.insert($0, after: nil)})
//        player.play()
//    }
//
//    private func getUrlSurahUrl(url: String, ayahInfo: AyahInfo) -> String {
//        let surahFiller = "\(currentAyah.surahNumber!)"
//        let ayahFiller = getAyahFiller(for: ayahInfo)
//        var finalUrl = url.replacingOccurrences(of: "{souraNumber}", with: surahFiller)
//        finalUrl = finalUrl.replacingOccurrences(of: "{000000}", with: ayahFiller)
//        return finalUrl
//    }
//
//    private func getAyahFiller(for ayahInfo: AyahInfo) -> String {
//        return "\(getThreeDigitString(for: ayahInfo.surahNumber))\(getThreeDigitString(for: ayahInfo.ayahNumber))"
//    }
//
//    private func getThreeDigitString(for number: Int) -> String {
//        if number < 10 {
//            return "00\(number)"
//        }
//
//        if number < 100 {
//            return "0\(number)"
//        }
//
//        return "\(number)"
//    }
//
//    @objc private func didFinishAyahRecitation() {
//
//        if let numberOfAyahsInsSurah = MushafDBManager.shared.getSurah(surahNumber: currentAyah.surahNumber).numberOfAyas {
//            percentageOfCurrentSurahChanged?(Float(currentAyah.ayahNumber) / Float(numberOfAyahsInsSurah))
//        }
//
//        guard let nextAyah = getNexttAyah(for: currentAyah) else { return }
//
//        self.currentAyah = nextAyah
//
//        if MushafDBManager.shared.getPage(ayahInfo: self.currentAyah) != currentPage {
//            currentPage = MushafDBManager.shared.getPage(ayahInfo: self.currentAyah)
//            playFromStart()
//        }
//
//
//    }
//
//    func pause() {
//        player.pause()
//    }
//
//    func stop() {
//
//    }
//
//    private func getNexttAyah(for ayahInfo: AyahInfo) -> AyahInfo? {
//        return MushafDBManager.shared.getNextAyah(ayahInfo: ayahInfo)
//    }
//
//}
