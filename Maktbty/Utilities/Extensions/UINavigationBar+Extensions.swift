//
//  UINavigationBar+Extensions.swift
//  GameOn
//
//  Created by Hassan on 2/3/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func adjustDefaultNavigationBar() {
//        self.barTintColor = mainBlueColor
        self.barTintColor =  #colorLiteral(red: 0.8392905593, green: 0.9799274802, blue: 0.9642708898, alpha: 1)
        
        self.setBackgroundImage(UIImage(), for: .default)
//        self.backgroundColor = mainBlueColor
        self.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.tintColor = UIColor.black

        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        
        self.isTranslucent = false
        self.shadowImage = UIImage()
    }
}
