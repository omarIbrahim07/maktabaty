////
////  CategoriesAPIManager.swift
////  Advertising808080
////
////  Created by Omar Ibrahim on 3/12/19.
////  Copyright © 2019 Advertising808080. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//class CategoriesAPIManager: BaseAPIManager {
//
//    var parentID: String?
//
//    func getMainCategorCategoriesAPIManageries(basicDictionary params:APIParams , onSuccess: @escaping ([MainCategories])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_MAIN_CATEGORIES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Arabic"] as? [[String : Any]] {
//                let wrapper = Mapper<MainCategories>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSubCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([SubCategories])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_SUB_CATEGORY = "\(GET_SUB_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_SUB_CATEGORY, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Arabic"] as? [[String : Any]] {
//                let wrapper = Mapper<SubCategories>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSecondaryCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_SECONDARY_CATEGORIES = "\(GET_SECONDARY_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_SECONDARY_CATEGORIES, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSpecialSecondaryCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Special_SECONDARY_CATEGORIES = "\(GET_SPECIAL_SECONDARY_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Special_SECONDARY_CATEGORIES, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["CatFeaturedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSimilarAdvertisements( postID: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Similar_Advertisements = "\(GET_SIMILAR_ADVERTISEMENTS_URL)\(postID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Similar_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["similarAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSavedAdvertisements( userID: Int, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Saved_Advertisements = "\(GET_SAVED_ADVERTISEMENTS_URL)\(userID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Saved_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["SavedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//    func getAdvertisementDetails(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping (AdvertisementDetailsModel) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Advertisement = "\(GET_ADVERTISEMENT_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Advertisement, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["Ad_Details"] as? [String : Any], let wrapper = Mapper<AdvertisementDetailsModel>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getAdvertisementDetailsByAdId(advertisementId: Int, basicDictionary params:APIParams , onSuccess: @escaping (AdvertisementDetailsModel) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Advertisement = "\(GET_ADVERTISEMENT_URL)\(advertisementId)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Advertisement, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["Ad_Details"] as? [String : Any], let wrapper = Mapper<AdvertisementDetailsModel>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getAllAdvertisements(basicDictionary params:APIParams , onSuccess: @escaping (Categories) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_ALL_ADVERTISEMENTS, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<Categories>().map(JSON: response) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getUserAdvertisements( userID: String, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_User_Advertisements = "\(GET_USER_ADVERTISEMENT_BY_USER_ID)\(userID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_User_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//        let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//        onSuccess(wrapper)
//        }
//
//        else {
//        let apiError = APIError()
//        onFailure(apiError)
//        }
//
//        }) { (apiError) in
//        onFailure(apiError)
//        }
//    }
//
//
//    func getAllMyAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Advertisements = "\(GET_ALL_MY_ADVERTISEMENTS)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getAllMySpecialAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Special_Advertisements = "\(GET_ALL_MY_SPECIAL_ADVERTISEMENTS)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Special_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["FeaturedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSoldAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Sold_Advertisements = "\(GET_SOLD_ADVERTISEMENTS_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Sold_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["SoldAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSearchedAdvertisements(basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
////        let get_All_My_Sold_Advertisements = "\(GET_SOLD_ADVERTISEMENTS_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_ADVERTISEMENTS_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Search Result"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getCountries( basicDictionary params: APIParams , onSuccess: @escaping ([Country]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_COUNTRIES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Countries"] as? [[String : Any]] {
//                let wrapper = Mapper<Country>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getCities(id: String, basicDictionary params: APIParams , onSuccess: @escaping ([City]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Cities_URL = "\(GET_CITIES_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Cities_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Cities"] as? [[String : Any]] {
//                let wrapper = Mapper<City>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getPostTypes(basicDictionary params: APIParams , onSuccess: @escaping ([PostType]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_POST_TYPES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Post Types"] as? [[String : Any]] {
//                let wrapper = Mapper<PostType>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getMainBanner(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_MAIN_BANNER_URL, parameters: params)
//
//        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["Main Banner"] as? String {
//
//                onSuccess(message)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//}
