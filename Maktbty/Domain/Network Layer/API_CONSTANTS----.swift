//
//  API_CONSTANTS.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation


//let MainURL = "http://new.808080group.com/api/"


let GET_MAIN_CATEGORIES_URL = "cats"
let GET_SUB_CATEGORIES_URL = "subcats/"
let GET_COUNTRIES_URL = "countries"
let GET_CITIES_URL = "cites/"
let GET_SECONDARY_CATEGORIES_URL = "ads/cat/"
let GET_SPECIAL_SECONDARY_CATEGORIES_URL = "ads/featured/cat/"
let GET_ADVERTISEMENT_URL = "ad/"

let GET_ALL_ADVERTISEMENTS = "ads"
let GET_ALL_SPECIAL_ADVERTISEMENTS = "ads/featured"
let GET_ALL_MY_ADVERTISEMENTS = "ads/user/"
let GET_ALL_MY_SPECIAL_ADVERTISEMENTS = "ads/featured/user/"
let GET_SIMILAR_ADVERTISEMENTS_URL = "similarads/ad/"
let GET_SAVED_ADVERTISEMENTS_URL = "user/savedads/"
let GET_USER_ADVERTISEMENT_BY_USER_ID = "ads/user/"
let GET_SOLD_ADVERTISEMENTS_URL = "ads/sold/user/"
let GET_SEARCHED_ADVERTISEMENTS_URL = "search"

let registerURL = "register"
let getUserURL = "user"

let FIRST_LEVEL_POST_ADVERTISEMENT_URL = "postad"
let EDIT_FIRST_LEVEL_POST_ADVERTISEMENT_URL = "editad/"
let CHANGE_PASSWORD_URL = "user/changepassword"
let CHANGE_USER_PROFILE_URL = "user/editprofile"
let SAVE_POST_URL = "ads/savead"
let CHECK_SAVED_POST_URL = "ads/issaved"
let DELETE_POST_URL = "ad/delete/"
let GET_POST_TYPES_URL = "posttypes"
let SELL_ADVERTISEMENT_URL = "setsold/ad/"

let SAVE_AD_PICS = "postpics"
let GET_MAIN_BANNER_URL = "mainbanner"

let GET_MESSAGES_URL = "messages"
let GET_CHAT_URL = "chat"
let GET_CHAT_MESSAGES_URL = "chat"
let TEXT_CHAT_MESSAGE_URL = "sendmsg"

let LOGIN_URL = "login"
let GET_INTERESTS_URL = "get-interests"
let GET_POSTS_URL = "get-posts"
let LOGOUT_URL = "logout"
let ADD_NEW_POST_URL = "add-post"
let GET_NOTIFICATION_URL = "get-notifications"
let GET_READ_POST_URL = "get-read-post"
let ADD_FOLLOW_POST = "add-follow-post"
let DELETE_POST = "delete-post"
let REPORT_POST = "add-feedback-post"
let DELETE_COMMENT = "delete-comment-post"
let DELETE_REPLY = "delete-comment-replies-post"
let LIKE_COMMENT = "add-like-comment-post"
let LIKE_REPLY = "add-like-comment-replies-post"
let GET_PROFILE_DATA = "profile"
