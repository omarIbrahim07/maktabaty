//
//  API_CONSTANTS..swift
//  Maktbty
//
//  Created by Gado on 7/8/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import Foundation


//let MainURL = "http://demo.maktbti.com/json/"
let MainURL = "http://maktbti.com/json/"

let GET_CATEGORIES_URL = "categories.php"
let GET_BOOKS_BY_CATEGORY_URL = "get_books.php"
let GET_BOOK_DETAILS_URL = "detailes_books.php"
let GET_AUTHORS_URL = "authers.php"
let GET_BOOKS_BY_TYPE_URL = "get_books_bytype.php"
let GET_AUTHOR_DETAILS_URL = "about_auther.php"
let GET_AUTHOR_COMMENTS_URL = "get_comment_author.php"
let GET_AUTHOR_SEARCH_URL = "get_auther_search.php"
let GET_BOOK_COMMENTS_URL = "get_comment.php"
let GET_HOME_SLIDER_URL = "all_ads_home.php"
let GET_ACCOUNT_DETAILS_URL = "account_detailes.php"
let GET_ACCOUNT_LOGIN_URL = "login.php"
let GET_SEARCHED_BOOKS_URL = "get_books_search.php"
let GET_SEARCHED_AUTHORS_URL = "get_auther_search.php"
let GET_FAVOURITE_BOOKS_URL = "my_favorite.php"
let GET_SARCHED_FAVOURITE_BOOKS_URL = "favorite_search.php"
let GET_ACCOUNT_SIGNUP_URL = "register.php"
let GET_POLICY_SUBSCRIPTION_PAGES_URL = "pages.php"
let EDIT_PROFILE_URL = "edit_profile.php"
let GET_USER_PROFILE = "account_detailes.php"
let FAVOURITE_URL = "favorites.php"
let UNFAVOURITE_URL = "favorites.php"
let POST_COMMENT_ON_BOOK_URL = "add_comment.php"
let POST_COMMENT_ON_AUTHOR_URL = "add_comment_author.php"
let RATE_AUTHOR_URL = "add_rating_author.php"
let RATE_BOOK_URL = "add_rating.php"
let GET_BASKET_URL = "basket.php"
let GET_MYBOOKS_URL = "get_books_paid.php"
let DELETE_BASKET_BOOK_URL = "delete_from_basket.php"
let ADD_BOOK_TO_BASKET_URL = "add_basket.php"
let GET_PAYMENT_URL_URL = "payment.php"
