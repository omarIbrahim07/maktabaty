import UIKit
import ObjectMapper

class AuthenticationAPIManager: BaseAPIManager {
    
    static var shared = AuthenticationAPIManager()
    var userLoggedIn: Bool {
        get {
            if let _ = UserDefaultManager.shared.currentUser {
                return true
            }
            else {
                return false
            }
        }
    }
    
    var fbData: [String : Any]?
    
    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let tokenType: String = response["token_type"] as? String, let accessToken: String = response["access_token"] as? String {
                let authorization = "\(tokenType) \(accessToken)"
                UserDefaultManager.shared.authorization = authorization
                onSuccess(authorization)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func registerUser(basicDictionary params:APIParams, onSuccess: @escaping () -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: registerURL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message = response["message"] as? String, message == "user created successfully" {
                onSuccess()
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "البريد الالكتروني مٌستخدم من قبل"
            }
            onFailure(apiError)
        }
    }
//
//    func getUserProfile(email: String, user_pass: String, onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_ACCOUNT_LOGIN_URL, parameters: ["email" : email as AnyObject, "user_pass" : user_pass as AnyObject])
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let userWrapper = Mapper<User>().map(JSON: response) {
//                UserDefaultManager.shared.currentUser = userWrapper
//                onSuccess(userWrapper)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//
//    }
    
    func login(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GET_ACCOUNT_LOGIN_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let id: String = response["ID"] as? String {
                
                onSuccess(id)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getUserProfile(basicDictionary params:APIParams, onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {

        let engagementRouter = BaseRouter(method: .get, path: GET_USER_PROFILE, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let userWrapper = Mapper<User>().map(JSON: response) {
                UserDefaultManager.shared.currentUser = userWrapper
        
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func changePassword(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func registerUser(basicDictionary params:APIParams, onSuccess: @escaping (RegisterUser) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_ACCOUNT_SIGNUP_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let userWrapper = Mapper<RegisterUser>().map(JSON: response) {
                //                UserDefaultManager.shared.currentUser = userWrapper
                
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func changeUserProfileWithImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)

        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
//                UserDefaultManager.shared.currentUser = userWrapper


                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func changeUserProfile(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: EDIT_PROFILE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                //                UserDefaultManager.shared.currentUser = userWrapper
                
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    
    //    func loginWithFacebook(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: LOGIN_FB_URL, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            if let isNewUser:Bool = (responseObject as! [String : Any])["is_new_user"] as? Bool, isNewUser {
    //                onSuccess(User())
    //            }
    //            else {
    //                let userWrapper = Mapper<UserData>().map(JSON: responseObject as! [String : Any])
    //                UserDefaultManager.shared.currentUser = userWrapper?.userData
    //                onSuccess((userWrapper?.userData)!)
    //            }
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func createUser(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: SIGNUP_URL, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //            let userWrapper = Mapper<UserData>().map(JSON: responseObject as! [String : Any])
    //            UserDefaultManager.shared.currentUser = userWrapper?.userData
    //            onSuccess((userWrapper?.userData)!)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func createUserWithFacebook(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: SIGNUP_FB, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //            let userWrapper = Mapper<UserData>().map(JSON: responseObject as! [String : Any])
    //            UserDefaultManager.shared.currentUser = userWrapper?.userData
    //            onSuccess((userWrapper?.userData)!)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func createUserWithFacebookWithNewImage(imageData imgData : Data ,basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: SIGNUP_FB, parameters: params)
    //
    //        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            let userWrapper = Mapper<UserData>().map(JSON: responseObject as! [String : Any])
    //            UserDefaultManager.shared.currentUser = userWrapper?.userData
    //            onSuccess((userWrapper?.userData)!)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func uploadProfileImage(imageData imgData : Data ,basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: UPLOAD_PROFILE_IMAGE_URL, parameters: params)
    //
    //        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            onSuccess(true)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func changePassword(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            onSuccess(true)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //    func editProfile(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
    //        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            let userWrapper = Mapper<UserData>().map(JSON: responseObject as! [String : Any])
    //            UserDefaultManager.shared.currentUser = userWrapper?.userData
    //            onSuccess((userWrapper?.userData)!)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func logout(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: LOG_OUT, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //            FBSDKLoginManager().logOut()
    //            UserDefaultManager.shared.currentUser = nil
    //            onSuccess(true)
    //
    //        }) { (apiError) in
    //            FBSDKLoginManager().logOut()
    //            UserDefaultManager.shared.currentUser = nil
    //            onFailure(apiError)
    //        }
    //    }
    //
    //    func forgotPassword(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void)
    //    {
    //        let engagementRouter = BaseRouter(method: .post, path: FORGOT_PASSWORD, parameters: params)
    //
    //        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
    //
    //            onSuccess(true)
    //
    //        }) { (apiError) in
    //            onFailure(apiError)
    //        }
    //    }
    
    
}

