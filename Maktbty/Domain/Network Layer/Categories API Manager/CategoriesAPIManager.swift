//
//  File.swift
//  Maktbty
//
//  Created by Gado on 7/9/19.
//  Copyright © 2019 Gado. All rights reserved.
//



import UIKit
import ObjectMapper

class CategoriesAPIManager: BaseAPIManager {
    
//    var parentID: String?
    
    func getPaymentURL(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_PAYMENT_URL_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let paymentURL: String = response["url_payment"] as? String {
                onSuccess(paymentURL)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getCategories(basicDictionary params:APIParams , onSuccess: @escaping ([Category])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_CATEGORIES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Category>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getPaidBooks(basicDictionary params:APIParams , onSuccess: @escaping ([BooksByCategory])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MYBOOKS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["books"] as? [[String : Any]] {
                let wrapper = Mapper<BooksByCategory>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    
    
    func getBooks(categoryId: Int, userId: Int, basicDictionary params:APIParams , onSuccess: @escaping ([BooksByCategory])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_BOOKS_BY_CATEGORY_URL, parameters: ["cat_id" : categoryId as AnyObject, "user_id" : userId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["books"] as? [[String : Any]] {
                let wrapper = Mapper<BooksByCategory>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBookDetails(basicDictionary params:APIParams , onSuccess: @escaping (BookDetails) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_BOOK_DETAILS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<BookDetails>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
//    func getBookDetails(basicDictionary params:APIParams , onSuccess: @escaping (BookDetails)->Void, onFailure: @escaping  (APIError)->Void) {
//
//
//        let engagementRouter = BaseRouter(method: .post, path: GET_BOOK_DETAILS_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<BookDetails>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    
    
    func getAuthors (basicDictionary params:APIParams , onSuccess: @escaping ([Authors])->Void, onFailure: @escaping  (APIError)->Void) {
        
        
        let engagementRouter = BaseRouter(method: .get, path: GET_AUTHORS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Authors>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    
    func getFreeSubscribtionSaleBooks(userId: Int,typeId: Int,basicDictionary params:APIParams , onSuccess: @escaping ([BooksByCategory])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_BOOKS_BY_TYPE_URL, parameters: ["user_id" : userId as AnyObject, "type" : typeId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["books"] as? [[String : Any]] {
                let wrapper = Mapper<BooksByCategory>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    func getAuthorDetails (authorId: Int, basicDictionary params:APIParams , onSuccess: @escaping (AuthorDetails)->Void, onFailure: @escaping  (APIError)->Void) {
        
        
        let engagementRouter = BaseRouter(method: .get, path: GET_AUTHOR_DETAILS_URL, parameters: ["id" : authorId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any],
                let wrapper = Mapper<AuthorDetails>().map(JSON: data){
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    func getAuthorCommentsPreviews (authorId: Int, basicDictionary params:APIParams , onSuccess: @escaping ([CommentsPreviews])->Void, onFailure: @escaping  (APIError)->Void) {
        
        
        let engagementRouter = BaseRouter(method: .get, path: GET_AUTHOR_COMMENTS_URL, parameters: ["author_id" : authorId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<CommentsPreviews>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBookCommentsPreviews (bookId: Int, basicDictionary params:APIParams , onSuccess: @escaping ([CommentsPreviews])->Void, onFailure: @escaping  (APIError)->Void) {
        
        
        let engagementRouter = BaseRouter(method: .get, path: GET_BOOK_COMMENTS_URL, parameters: ["book_id" : bookId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<CommentsPreviews>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getHomeSlider (basicDictionary params:APIParams , onSuccess: @escaping ([HomeSlider])->Void, onFailure: @escaping  (APIError)->Void) {
        
        
        let engagementRouter = BaseRouter(method: .get, path: GET_HOME_SLIDER_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<HomeSlider>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func searchForBooks(basicDictionary params:APIParams , onSuccess: @escaping ([SearchedBook])->Void, onFailure: @escaping  (APIError)->Void) {
//
//            let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_BOOKS_URL, parameters: params)
//
//            super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["books"] as? [[String : Any]] {
//                let wrapper = Mapper<SearchedBook>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func searchForBooks(basicDictionary params:APIParams , onSuccess: @escaping ([SearchedBook])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_BOOKS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["books"] as? [[String : Any]] {
                let wrapper = Mapper<SearchedBook>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func searchForAuthors(basicDictionary params:APIParams , onSuccess: @escaping ([Authors])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_AUTHORS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Authors>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getFavouriteBooks(basicDictionary params:APIParams , onSuccess: @escaping ([SearchedBook])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_FAVOURITE_BOOKS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<SearchedBook>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBasketBooks(basicDictionary params:APIParams , onSuccess: @escaping ([BasketBook], Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_BASKET_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]], let total: Int = response["total"] as? Int {
                let wrapper = Mapper<BasketBook>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper, total)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addBookToBasket(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: ADD_BOOK_TO_BASKET_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let status: Bool = response["status"] as? Bool {
                onSuccess(status)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func deleteBasketBook(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: DELETE_BASKET_BOOK_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let status: Bool = response["status"] as? Bool {
                onSuccess(status)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }


    
    func favouriteBook(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: FAVOURITE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func unfavouriteBook(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: UNFAVOURITE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func postCommentOnBook(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: POST_COMMENT_ON_BOOK_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func postCommentOnAuthor(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: POST_COMMENT_ON_AUTHOR_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func rateAuthor(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: RATE_AUTHOR_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func rateBook(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: RATE_BOOK_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                onSuccess(message)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func getAdvertisementDetails(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping (AdvertisementDetailsModel) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Advertisement = "\(GET_ADVERTISEMENT_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Advertisement, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["Ad_Details"] as? [String : Any], let wrapper = Mapper<AdvertisementDetailsModel>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//    func getSubCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([SubCategories])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_SUB_CATEGORY = "\(GET_SUB_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_SUB_CATEGORY, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Arabic"] as? [[String : Any]] {
//                let wrapper = Mapper<SubCategories>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSecondaryCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_SECONDARY_CATEGORIES = "\(GET_SECONDARY_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_SECONDARY_CATEGORIES, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSpecialSecondaryCategories(Parent: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Special_SECONDARY_CATEGORIES = "\(GET_SPECIAL_SECONDARY_CATEGORIES_URL)\(Parent)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Special_SECONDARY_CATEGORIES, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["CatFeaturedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSimilarAdvertisements( postID: String, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Similar_Advertisements = "\(GET_SIMILAR_ADVERTISEMENTS_URL)\(postID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Similar_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["similarAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSavedAdvertisements( userID: Int, basicDictionary params:APIParams , onSuccess: @escaping ([AdvertisementDetailsModel])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Saved_Advertisements = "\(GET_SAVED_ADVERTISEMENTS_URL)\(userID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Saved_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["SavedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//

//
//    func getAdvertisementDetailsByAdId(advertisementId: Int, basicDictionary params:APIParams , onSuccess: @escaping (AdvertisementDetailsModel) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Advertisement = "\(GET_ADVERTISEMENT_URL)\(advertisementId)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Advertisement, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["Ad_Details"] as? [String : Any], let wrapper = Mapper<AdvertisementDetailsModel>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getAllAdvertisements(basicDictionary params:APIParams , onSuccess: @escaping (Categories) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_ALL_ADVERTISEMENTS, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<Categories>().map(JSON: response) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getUserAdvertisements( userID: String, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_User_Advertisements = "\(GET_USER_ADVERTISEMENT_BY_USER_ID)\(userID)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_User_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//    func getAllMyAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Advertisements = "\(GET_ALL_MY_ADVERTISEMENTS)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Ads"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getAllMySpecialAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Special_Advertisements = "\(GET_ALL_MY_SPECIAL_ADVERTISEMENTS)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Special_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["FeaturedAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSoldAdvertisements(id: Int, basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_All_My_Sold_Advertisements = "\(GET_SOLD_ADVERTISEMENTS_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_All_My_Sold_Advertisements, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["SoldAds"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getSearchedAdvertisements(basicDictionary params: APIParams , onSuccess: @escaping ([AdvertisementDetailsModel]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        //        let get_All_My_Sold_Advertisements = "\(GET_SOLD_ADVERTISEMENTS_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_ADVERTISEMENTS_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Search Result"] as? [[String : Any]] {
//                let wrapper = Mapper<AdvertisementDetailsModel>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getCountries( basicDictionary params: APIParams , onSuccess: @escaping ([Country]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_COUNTRIES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Countries"] as? [[String : Any]] {
//                let wrapper = Mapper<Country>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getCities(id: String, basicDictionary params: APIParams , onSuccess: @escaping ([City]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let get_Cities_URL = "\(GET_CITIES_URL)\(id)"
//
//        let engagementRouter = BaseRouter(method: .get, path: get_Cities_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Cities"] as? [[String : Any]] {
//                let wrapper = Mapper<City>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getPostTypes(basicDictionary params: APIParams , onSuccess: @escaping ([PostType]) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_POST_TYPES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Post Types"] as? [[String : Any]] {
//                let wrapper = Mapper<PostType>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getMainBanner(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_MAIN_BANNER_URL, parameters: params)
//
//        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["Main Banner"] as? String {
//
//                onSuccess(message)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    
}

