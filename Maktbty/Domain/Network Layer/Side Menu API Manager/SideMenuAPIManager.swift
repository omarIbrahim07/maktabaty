//
//  SideMenuAPIManager.swift
//  Maktbty
//
//  Created by Gado on 7/28/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit
import ObjectMapper

class SideMenuAPIManager: BaseAPIManager {

    
    func getAccountDetails (userId:Int, basicDictionary params:APIParams , onSuccess: @escaping (AccountDetails)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_ACCOUNT_DETAILS_URL, parameters: [ "ID" : userId as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any],
                let wrapper = Mapper<AccountDetails>().map(JSON: response){
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
//    func getCategories(basicDictionary params:APIParams , onSuccess: @escaping ([Category])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_CATEGORIES_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
//                let wrapper = Mapper<Category>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//
//
//
//    func getBookDetails (bookId: Int, userId: Int, basicDictionary params:APIParams , onSuccess: @escaping (BookDetails)->Void, onFailure: @escaping  (APIError)->Void) {
//
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_BOOK_DETAILS_URL, parameters: ["id" : bookId as AnyObject, "user_id" : userId as AnyObject])
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any],
//                let wrapper = Mapper<BookDetails>().map(JSON: data){
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func getSubscriptionsDetailsScreen (id:Int, basicDictionary params:APIParams , onSuccess: @escaping (Subscribtion)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_POLICY_SUBSCRIPTION_PAGES_URL, parameters: [ "id" : id as AnyObject])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any],
                let wrapper = Mapper<Subscribtion>().map(JSON: response){
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    
    
}

